<h1 align="center">
  <br>
  SimpleDARS
  <br>
</h1>
<h3 align="center">
Your simple and secure DARS wallet.
</h3>

# About

SimpleDARS is a wallet made solely for the DARS ecosystem and fully integrated with all features available in the DARS software.
 
DARS made SimpleDARS with a security and transparency philosophy. It is a desktop application compatible with the most popular operation systems (Windows, Linux and MacOS).

User experience focus was also a main part of SimpleDARS creation process.  

## Warning

Only download SimpleDARS from DARS [website](https://dars.one/SimpleDARS/) or [github](https://github.com/darsblockchain/simpledars). Avoid scams, do not trust any other source.

Distribution or copy of this software or any of its parts and associated documentation, is not allowed by applicable law, unless previous written permission is given by DARS. All rights are reserved.

SimpleDARS doesn't keep any of your information. All information is kept locally only, not in any cloud services or databases.

## Main Features
As of 12/06/2018, SimpleDARS main features include:

- Local Storage: private keys are encrypted and stored locally only.
- Multiple accounts support
- Token transfer
- All transactions are password protected
- Contacts List
- Transactions / Actions History
- Voting Portal
- Stake / Unstake functions - (un)delegatebw
- Support for generic tokens (airdrops)
- Custom endpoints on the mainnet
- Create new account

## Roadmap

Next features to be implemented:
- Testnets support
- Delegate Permissions
- Setup Delayed Transactions
- Mobile Version
- Multi languages support

## Security Measures
### Encryption & Local storage only
Your private keys are stored locally only and are properly encrypted with a user defined password of 10+ characters.

### Proxy to fetch external data
Off-chain information (such as the Block Producer standard) are fetched via a proxy server provided by DARS Rio, to avoid malformed json data and third-party servers misconfigurations. 

## Download a pre-compiled build
- [Windows](https://github.com/darsblockchain/simpledars/releases/download/v0.6.9/SimpleDARS.Setup.0.6.9.exe)
  - `e38306e9566d436d303a4b1ea98a76b946112df428bcba54f65820d1f02c7208  SimpleDARS.Setup.0.6.9.exe`
- [MacOS](https://github.com/darsblockchain/simpledars/releases/download/v0.6.9/SimpleDARS-0.6.9.dmg)
  - `253a12d6fc7e149922b4931faf2257d5025eedd9f5e8a7d9a42494b5e9750fb4  SimpleDARS-0.6.9.dmg`
- [Linux AppImage](https://github.com/darsblockchain/simpledars/releases/download/v0.6.9/SimpleDARS-0.6.9-x86_64.AppImage)
  - `ad0e0d74f92d43473f41d22b8d5448f45a8efb2eb6b4fec6e3a87610da79eeda  SimpleDARS-0.6.9-x86_64.AppImage`
- [Linux DEB](https://github.com/darsblockchain/simpledars/releases/download/v0.6.9/SimpleDARS_0.6.9_amd64.deb)
  - `10191d3752303487dbf41d5bdb34b696024647e520cbc20b197ba15693583b44  SimpleDARS_0.6.9_amd64.deb`

## Legal Disclaimer

By downloading SimpleDARS you agree to the [Terms of Service](https://dars.one/terms-of-service/).

## Build it yourself

[Yarn](http://yarnpkg.com/) is [strongly](https://github.com/electron-userland/electron-builder/issues/1147#issuecomment-276284477) recommended instead of npm.

### Dependencies Setup

#### Windows
- [Node.js](https://nodejs.org/en/download/current/)
- [Yarn](https://yarnpkg.com/en/docs/install#windows-stable)

#### Ubuntu
```
# Install Node.js 10
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential

# Install Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

#### Fedora / Red Hat®
```
# Install Node.js 10
curl --silent --location https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum -y install nodejs
sudo yum install gcc-c++ make

# Install Yarn
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
sudo yum install yarn
```

#### MacOS
```
# Install brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Node.js 10
brew install node

# Install Yarn
brew install yarn
```

## Setup sources:
```console
git clone https://github.com/darsblockchain/simpledars.git
cd SimpleDARS
yarn install
yarn run build:prod
```
Create installer:
```
yarn dist
```
The packages will be available on the `/dist` folder.

## Further help

To get more help please contact our team at contact@DARSrio.io or at our [Telegram channel](https://t.me/DARSrio).
