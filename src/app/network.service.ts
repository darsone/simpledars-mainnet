import {Injectable} from '@angular/core';
import {AccountsService} from './accounts.service';
import {DARSJSService} from './darsjs.service';
import {Router} from '@angular/router';

import * as DARS from '../assets/eos.js';
import {BehaviorSubject} from 'rxjs';

export interface Endpoint {
  url: string;
  owner: string;
  latency: number;
  filters: string[];
}

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  publicEndpoints: Endpoint[];
  dars: any;
  mainnetId = 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f';
  genesistx = 'ad77575a8b4f52e477682e712b1cbd884299468db6a94d909f90c6961cea9b02';
  voteref = 'b23f537e8ab29fbcec8b533081ef7e12b146899ca42a3fc9eb608258df9983d9';
  txrefBlock = 191;
  voterefBlock = 572278;
  baseConfig = {
    httpEndpoint: '',
    expireInSeconds: 60,
    broadcast: true,
    debug: false,
    sign: true,
    chainId: ''
  };
  validEndpoints: Endpoint[];
  status: string;
  connectionTimeout: any;
  selectedEndpoint = new BehaviorSubject<Endpoint>(null);
  networkingReady = new BehaviorSubject<boolean>(false);

  connected = false;

  constructor(private darsjs: DARSJSService, private router: Router, public aService: AccountsService) {
    this.publicEndpoints = [
      {url: 'http://159.69.176.220:8888', owner: 'DARS', latency: 0, filters: []}
//      {url: 'https://testproducer.dars.one', owner: 'DARS', latency: 0, filters: []}
    ];
    this.validEndpoints = [];
    this.status = '';
    this.connectionTimeout = null;
  }

  connect() {
    this.status = '';
    this.networkingReady.next(false);

    const pQueue = [];
    this.connected = false;

//console.log('!!!network.service:connect:Starting connection timer...');
    this.startTimeout();
	
    this.publicEndpoints.forEach((apiNode) => {
      pQueue.push(this.apiCheck(apiNode));
    });

    Promise.all(pQueue).then(() => {
      this.extractValidNode();
    });
  }

  startTimeout() {
    this.connectionTimeout = setTimeout(() => {
//console.log('!!!network.service:startTimeout:Connection timeout!');
      if (!this.networkingReady.getValue()) {
        this.status = 'timeout';
        clearTimeout(this.connectionTimeout);
        this.networkingReady.next(false);
        this.connectionTimeout = null;
      }
    }, 10000);
  }

  async scanNodes() {
    for (const apiNode of this.publicEndpoints) {
      await this.apiCheck(apiNode);
    }
  }

  extractValidNode() {
    for (const node of this.publicEndpoints) {
      this.validEndpoints.push(node);
      // if (node.filters.length === 2) {
      //   this.validEndpoints.push(node);
      // }
    }
    this.selectEndpoint();
  }

  selectEndpoint() {
    let latency = 5000;
    this.validEndpoints.forEach((node) => {
      if (node.latency < latency && node.latency > 1) {
        latency = node.latency;
        this.selectedEndpoint.next(node);
      }
    });
    if (this.selectedEndpoint.getValue() === null) {
      this.networkingReady.next(false);
    } else {
//console.log('!!!network.service:selectEndpoint:Best Server Selected!', this.selectedEndpoint.getValue().url);
      this.startup(null);
    }
  }

  async verifyFilters() {
    for (const apiNode of this.publicEndpoints) {
      if (apiNode.latency > 0 && apiNode.latency < 1000) {
        await this.filterCheck(apiNode);
      }
    }
  }

  filterCheck(server: Endpoint) {
    console.log('Starting filter check for ' + server.url);
    const config = this.baseConfig;
    config.httpEndpoint = server.url;
    config.chainId = this.mainnetId;
    const dars = DARS(config);
    const pq = [];
    pq.push(new Promise((resolve1) => {
      dars['getTransaction'](this.genesistx, (err, txInfo) => {
        if (err) {
          console.log(err);
          resolve1();
        } else {
          if (txInfo['block_num'] === this.txrefBlock) {
            server.filters.push('dars.token:transfer');
          } else {
            console.log('dars.token:transfer filter is disabled on ' + server.url);
          }
          resolve1();
        }
      });
    }));
    pq.push(new Promise((resolve1) => {
      dars['getTransaction'](this.voteref, (err, txInfo) => {
        if (err) {
          console.log(err);
          resolve1();
        } else {
          if (txInfo['block_num'] === this.voterefBlock) {
            server.filters.push('dars:voteproducer');
          } else {
            console.log('dars:voteproducer filter is disabled on ' + server.url);
          }
          resolve1();
        }
      });
    }));
    return Promise.all(pq);
  }

  apiCheck(server: Endpoint) {
//console.log('!!!network.service:apiCheck:Starting latency check for ' + server.url);
    return new Promise((resolve) => {
      const config = this.baseConfig;
      config.httpEndpoint = server.url;
      config.chainId = this.mainnetId;
      const dars = DARS(config);
      const refTime = new Date().getTime();
      const tempTimer = setTimeout(() => {
        server.latency = -1;
        resolve();
      }, 5000);
      try {
        dars['getInfo']({}, (err) => {
          if (err) {
            server.latency = -1;
          } else {
            server.latency = ((new Date().getTime()) - refTime);
//console.log('!!!network.service:apiCheck:Latency for '+server.url, ' is ', server.latency);
          }
          clearTimeout(tempTimer);
          if (server.latency > 1 && server.latency < 2000) {
//console.log('!!!network.service:apiCheck:Force quick connection to '+server.url);
            // force quick connection
            if (this.connected === false) {
              this.connected = true;
              this.selectedEndpoint.next(server);
              this.startup(null);
            }
          }
          resolve();
        });
      } catch (e) {
        server.latency = -1;
        resolve();
      }
    });
  }

  startup(url) {
//console.log('!!!network.service:startup:url='+url);
    let endpoint = url;
    if (!url) {
      endpoint = this.selectedEndpoint.getValue().url;
    } else {
      this.status = '';
      this.networkingReady.next(false);
      this.startTimeout();
    }

    this.darsjs.init(endpoint, this.mainnetId).then((savedAccounts: any) => {
      if (this.connectionTimeout) {
        clearTimeout(this.connectionTimeout);
        this.networkingReady.next(true);
        this.connectionTimeout = null;
      }
      if (savedAccounts) {
        if (savedAccounts.length > 0) {
//console.log('!!!network.service:startup:Has saved accounts!');
          this.aService.loadLocalAccounts(savedAccounts);
          this.aService.initFirst();
          this.networkingReady.next(true);
//console.log('!!!network.service:startup:Navigate to dashboard->send!');
          this.router.navigate(['dashboard', 'send']);
        } else {
//console.log('!!!network.service:startup:No saved accounts -> no navigate!');
        }
      }
    }).catch(() => {
      this.networkingReady.next(false);
    });
  }

}
