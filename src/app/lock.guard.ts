import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {CryptoService} from './services/crypto.service';

@Injectable({
  providedIn: 'root'
})
export class LockGuard implements CanActivate {

  constructor(private crypto: CryptoService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('simpledars-hash')) {
//console.log("!!!lock.guard.simpledars-hash!=null"); 
      if (this.crypto.locked) {
//console.log("!!!lock.guard.crypto.locked ->cant navigate"); 
        this.router.navigate(['']);
        return false;
      } else {
//console.log("!!!lock.guard.crypto.unlocked -> can navigate"); 
        return true;
      }
    } else {
//console.log("!!!lock.guard.simpledars-hash===null -> can navigate"); 
     return true;
    }
  }
}
