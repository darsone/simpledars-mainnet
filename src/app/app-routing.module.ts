import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LandingComponent} from './landing/landing.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {WalletComponent} from './dashboard/wallet/wallet.component';
import {RentComponent} from './dashboard/rent/rent.component';
import {BonusComponent} from './dashboard/bonus/bonus.component';
import {HistoryComponent} from './dashboard/history/history.component';
import {VoteComponent} from './dashboard/vote/vote.component';
import {CommunityComponent} from './dashboard/community/community.component';
import {SendComponent} from './dashboard/send/send.component';
import {ConfigComponent} from './dashboard/settings/config.component';
import {AboutComponent} from './dashboard/about/about.component';
import {LockscreenComponent} from './lockscreen/lockscreen.component';
import {LockGuard} from './lock.guard';

const routes: Routes = [
  {
    path: '',
    component: LockscreenComponent
  },
  {
    path: 'landing',
    component: LandingComponent,
    canActivate: [LockGuard]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [LockGuard],
    children: [
      {
        path: 'wallet',
        component: WalletComponent,
      },
      {
        path: 'send',
        component: SendComponent,
      },
      {
        path: 'rent',
        component: RentComponent,
      },
      {
        path: 'bonus',
        component: BonusComponent,
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
      {
        path: 'vote',
        component: VoteComponent,
      },
      {
        path: 'community',
        component: CommunityComponent,
      },
      {
        path: 'config',
        component: ConfigComponent,
      },
      {
        path: 'about',
        component: AboutComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
