import {Component, OnInit} from '@angular/core';
import {CryptoService} from '../services/crypto.service';
import {Router} from '@angular/router';
import {NetworkService} from '../network.service';

@Component({
    selector: 'app-lockscreen',
    templateUrl: './lockscreen.component.html',
    styleUrls: ['./lockscreen.component.css']
})
export class LockscreenComponent implements OnInit {

    pin = '';
    nAttempts = 5;
    wrongpass = false;
    logoutModal: boolean;
    clearContacts: boolean;
    anim: any;
    lottieConfig: Object;


    constructor(private crypto: CryptoService, private router: Router, private network: NetworkService) {
        this.logoutModal = false;
        this.clearContacts = false;
    }

    ngOnInit() {
//console.log("!!!lockscreen.ngoninit");
        if (localStorage.getItem('simpledars-hash') === null) {
//console.log("!!!lockscreen.simpledars-hash===null -> navigate to 'landing'");
	    //Если не установлен пин-код - сразу переходим на страницу "landing"
            this.router.navigate(['landing']).catch(() => {
		console.log('!!!lockscreen.cannot navigate to landing');
	    });
        }
    }

    handleAnimation(anim: any) {
        this.anim = anim;
        this.anim['setSpeed'](0.8);
    }

    unlock() {
        let target = ['landing'];
        if (this.network.networkingReady.getValue()) {
            target = ['dashboard', 'send'];
        }
        if (!this.crypto.unlock(this.pin, target)) {
            this.wrongpass = true;
            this.nAttempts--;
            if (this.nAttempts === 0) {
                localStorage.clear();
                this.resetApp();
            }
        }
    }

    resetApp() {
	this.router.navigate(['landing']).catch((err) => {
	    console.log(err);
	});
    }

    logout() {
        if (this.clearContacts) {
            localStorage.clear();
        } else {
            const arr = [];
            for (let i = 0; i < localStorage.length; i++) {
                if (localStorage.key(i) !== 'simpledars.contacts') {
                    arr.push(localStorage.key(i));
                }
            }
            arr.forEach((k) => {
                localStorage.removeItem(k);
            });
        }
        this.resetApp();
    }

}
