import {AfterViewInit, Component} from '@angular/core';
import {NetworkService} from './network.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit {

  busy: boolean;

  constructor(
    public network: NetworkService,
  ) {
    this.busy = false;
  }

  ngAfterViewInit() {
    setTimeout(()=>{
      this.network.connect();
    }, 1000);
  }
}
