export interface DARSAccount {
  details: any;
  unstaked: number;
  unstaking: number;
  drs_balance: number;
  dusd_balance: number;
  staked: number;
  unstakeTime: string;
}
