﻿import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountsService} from '../../accounts.service';
import {DARSJSService} from '../../darsjs.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {CryptoService} from '../../services/crypto.service';
import {DARSAccount} from '../../interfaces/account';

import * as moment from 'moment';

export interface Contact {
  name: string;
  type: string;
  account?: string;
  default_memo?: string;
}

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css'],
})
export class SendComponent implements OnInit {
  contacts: Contact[];
  sendForm: FormGroup;
  contactForm: FormGroup;
  searchForm: FormGroup;
  confirmForm: FormGroup;
  sendModal: boolean;
  newContactModal: boolean;
  editContactModal: boolean;
  deleteContactModal: boolean;
  accountvalid: boolean;
  busy: boolean;
  add: boolean;
  needRent: boolean;
  errormsg: string;
  adderrormsg: string;
  amounterror: string;
  wrongpass: string;
  DRSBalance: number;
  DUSDBalance: number;
  selectedTokenName: string;
  staked: number;
  unstaked: number;
  unstaking: number;
  unstakeTime: string;
  contactExist: boolean;
  search: string;
  filteredContacts: Observable<Contact[]>;
  searchedContacts: Observable<Contact[]>;
  numberMask = createNumberMask({
    prefix: '',
    allowDecimal: true,
    includeThousandsSeparator: false,
    decimalLimit: 8,
  });
  config: ToasterConfig;
  fromAccount: string;
  token_balance = 0.0000;
  selectedEditContact = null;

  knownExchanges = [];
  memoMsg = 'необязательно';

  constructor(private fb: FormBuilder,
              public aService: AccountsService,
              public dars: DARSJSService,
              private crypto: CryptoService,
              private toaster: ToasterService) {
    this.sendModal = false;
    this.newContactModal = false;
    this.contactExist = true;
    this.fromAccount = '';
    this.busy = false;
    this.sendForm = this.fb.group({
      token: ['DRS', Validators.required],
      to: ['', Validators.required],
      amount: ['', Validators.required],
	  comission: [''],
      memo: [''],
      add: [false],
      alias: [''],
    });
    this.contactForm = this.fb.group({
      account: ['', Validators.required],
      name: ['', Validators.required],
    });
    this.searchForm = this.fb.group({
      search: ['']
    });
    this.confirmForm = this.fb.group({
	  dorent: [''],
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
    this.contacts = [];
    this.loadContacts();
    this.sortContacts();
    this.errormsg = '';
    this.adderrormsg = '';
    this.amounterror = '';
    this.wrongpass = '';
    this.accountvalid = false;
    this.unstaking = 0;
    this.unstakeTime = '';
    this.selectedTokenName = 'DRS';
  }

  filter(val: string, indexed): Contact[] {
    return this.contacts.filter(contact => {
      if (contact.type === 'contact') {
        return contact.name.toLowerCase().includes(val.toLowerCase()) || contact.account.toLowerCase().includes(val.toLowerCase());
      } else {
        if (indexed) {
          return contact.type === 'letter';
        } else {
          return false;
        }
      }
    });
  }

  checkExchangeAccount() {
    const memo = this.sendForm.get('memo');
    const acc = this.sendForm.get('to').value;
    if (this.knownExchanges.includes(acc)) {
      this.memoMsg = 'обязательно';
      memo.setValidators([Validators.required]);
      memo.updateValueAndValidity();
    } else {
      this.memoMsg = 'необязательно';
      memo.setValidators(null);
      memo.updateValueAndValidity();
    }
  }

  ngOnInit() {
    this.aService.selected.asObservable().subscribe((sel: DARSAccount) => {
      if (sel) {
        this.DRSBalance = sel.drs_balance;
        this.DUSDBalance = sel.dusd_balance;
        this.staked = sel.staked;
        this.unstaked = sel.drs_balance - sel.staked - sel.unstaking;
        this.unstaking = sel.unstaking;
        this.unstakeTime = moment.utc(sel.unstakeTime).add(72, 'hours').fromNow();
      }
    });
	
    this.sendForm.get('token').valueChanges.subscribe((symbol) => {
      this.sendForm.patchValue({
        amount: ''
      });
      if (symbol === 'DRS') {
        this.selectedTokenName = 'DRS';
      } else 
      if (symbol === 'DUSD') {
        this.selectedTokenName = 'DUSD';
      }
    });
	
    this.filteredContacts = this.sendForm.get('to').valueChanges.pipe(startWith(''), map(value => this.filter(value, false)));
    this.searchedContacts = this.searchForm.get('search').valueChanges.pipe(startWith(''), map(value => this.filter(value, true)));
    this.onChanges();
  }

  onChanges(): void {
    this.sendForm.get('add').valueChanges.subscribe(val => {
      this.add = val;
    });
  }

  checkContact(value) {
    this.checkExchangeAccount();
    const found = this.contacts.findIndex((el) => {
      return el.account === value;
    });
    this.contactExist = found === -1;
  }

  setMax() {
	let max = 0;
    if (this.sendForm.get('token').value === 'DRS') {
	 max = this.DRSBalance;
	} else		
    if (this.sendForm.get('token').value === 'DUSD') {
	 max = this.DUSDBalance;
	} else {
	 max = this.token_balance;
	}
    this.sendForm.patchValue({
      amount: max
    });
  }

  checkAmount() {
    if (parseFloat(this.sendForm.value.amount) === 0 || this.sendForm.value.amount === '') {
      this.sendForm.controls['amount'].setErrors({'incorrect': true});
      this.amounterror = 'неверное количество';
    } else {
	  let max = 0;
      if (this.sendForm.get('token').value === 'DRS') {
	   max = this.DRSBalance;
	  } else		
      if (this.sendForm.get('token').value === 'DUSD') {
	   max = this.DUSDBalance;
	  } else {
	   max = this.token_balance;
	  }

      if (parseFloat(this.sendForm.value.amount) > max) {
        this.sendForm.controls['amount'].setErrors({'incorrect': true});
        this.amounterror = 'неверное количество';
      } else {
        this.sendForm.controls['amount'].setErrors(null);
        this.amounterror = '';
      }
    }
  }

  checkAccountName() {
    if (this.sendForm.value.to !== '') {
	  if (this.sendForm.value.to == this.aService.selected.getValue().name) {
       this.sendForm.controls['to'].setErrors({'incorrect': true});
       this.errormsg = 'нельзя отправлять токены самому себе';
      } else {		  
       try {
         this.dars.checkAccountName(this.sendForm.value.to.toLowerCase());
         this.sendForm.controls['to'].setErrors(null);
         this.errormsg = '';
         this.dars.getAccountInfo(this.sendForm.value.to.toLowerCase()).then(() => {
           this.sendForm.controls['to'].setErrors(null);
           this.errormsg = '';
         }).catch(() => {
           this.sendForm.controls['to'].setErrors({'incorrect': true});
           this.errormsg = 'кошелек не существует';
         });
       } catch (e) {
         this.sendForm.controls['to'].setErrors({'incorrect': true});
         this.errormsg = e.message;
       }
	  }
    } else {
      this.errormsg = '';
    }
  }

  checkAccountModal() {
    if (this.contactForm.value.account !== '') {
      try {
        this.dars.checkAccountName(this.contactForm.value.account.toLowerCase());
        this.contactForm.controls['account'].setErrors(null);
        this.adderrormsg = '';
        this.dars.getAccountInfo(this.contactForm.value.account.toLowerCase()).then(() => {
          this.contactForm.controls['account'].setErrors(null);
          this.adderrormsg = '';
        }).catch(() => {
          this.contactForm.controls['account'].setErrors({'incorrect': true});
          this.adderrormsg = 'кошелек не существует';
        });
      } catch (e) {
        this.contactForm.controls['account'].setErrors({'incorrect': true});
        this.adderrormsg = e.message;
      }
    } else {
      this.adderrormsg = '';
    }
  }

  insertNewContact(data, silent) {
    const idx = this.contacts.findIndex((item) => {
      return item.account === data.account;
    });
    if (idx === -1) {
      this.contacts.push(data);
      this.contactForm.reset();
      this.searchForm.patchValue({
        search: ''
      });
    }
  }

  addAccountsAsContacts() {
    this.aService.accounts.map(acc => this.insertNewContact({
      type: 'contact',
      name: acc.name,
      account: acc.name
    }, true));
    this.addDividers();
    this.storeContacts();
  }

  removeDividers() {
    this.contacts.forEach((contact, idx) => {
      if (contact.type === 'letter') {
        this.contacts.splice(idx, 1);
      }
    });
  }

  addDividers() {
    this.removeDividers();
    const divs = [];
    this.contacts.forEach((contact) => {
      if (contact.type === 'contact') {
        const letter = contact.name.charAt(0).toUpperCase();
        if (!divs.includes(letter)) {
          divs.push(letter);
          const index = this.contacts.findIndex((item) => {
            return item.name === letter;
          });
          if (index === -1) {
            this.contacts.push({
              type: 'letter',
              name: letter
            });
            this.contactForm.reset();
            this.searchForm.patchValue({
              search: ''
            });
          }
        }
      }
    });
    this.sortContacts();
  }

  addContact() {
    try {
      this.dars.checkAccountName(this.contactForm.value.account.toLowerCase());
      this.dars.getAccountInfo(this.contactForm.value.account.toLowerCase()).then(() => {
        this.insertNewContact({
          type: 'contact',
          name: this.contactForm.value.name,
          account: this.contactForm.value.account.toLowerCase()
        }, false);
        this.newContactModal = false;
        this.addDividers();
        this.storeContacts();
      }).catch((error) => {
        alert(JSON.parse(error.message).error.details[0].message);
      });
    } catch (e) {
      alert('invalid account name!');
      console.log(e);
    }
  }

  addContactOnSend() {
    try {
      this.dars.checkAccountName(this.sendForm.value.to.toLowerCase());
      this.dars.getAccountInfo(this.sendForm.value.to.toLowerCase()).then(() => {
        this.insertNewContact({
          type: 'contact',
          name: this.sendForm.value['alias'],
          account: this.sendForm.value.to.toLowerCase()
        }, false);
        this.addDividers();
        this.storeContacts();
      }).catch((error) => {
        alert(JSON.parse(error.message).error.details[0].message);
      });
    } catch (e) {
      alert('invalid account name!');
      console.log(e);
    }
  }

  sortContacts() {
    this.contacts.sort((a, b) => {
      return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
    });
    this.searchForm.patchValue({
      search: ''
    });
  }

  selectContact(contact) {
    this.contactExist = true;
    this.sendForm.patchValue({
      to: contact.account,
      alias: contact.name
    });
  }

  storeContacts() {
    localStorage.setItem('simpledars.contacts', JSON.stringify(this.contacts));
  }

  loadContacts() {
    const contacts = localStorage.getItem('simpledars.contacts');
    if (contacts) {
      this.contacts = JSON.parse(contacts);
    } else {
      this.addAccountsAsContacts();
    }
  }
  
  parseDRS(tk_string) {
    if (tk_string.split(' ')[1] === 'DRS') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }
  
  openSendModal() {
    this.confirmForm.reset();
    this.fromAccount = this.aService.selected.getValue().name;

    this.dars.getComissions().then((comissions) => {
		let needDRSAmount = 0;
	 
		let needDRSComission = 0;
		if (comissions.rows.length > 0) {
			comissions.rows.forEach((line) => {
				if (line["command_name"] == "transferTokens") {
					needDRSComission = this.parseDRS(line["fee"]);
				}
			});
		}
		this.sendForm.patchValue({
			comission: needDRSComission
		});

		if (this.sendForm.get('token').value === 'DRS') {
			needDRSAmount = needDRSComission + parseFloat(this.sendForm.get('amount').value);
		} else {
			needDRSAmount = needDRSComission;
		}
		if (needDRSAmount > this.DRSBalance) {
			this.needRent = true;
		} else {
			this.needRent = false;
		}
	
		this.sendModal = true;
    }).catch((err) => {
	});
  }

  transfer() {
    this.checkExchangeAccount();
    this.busy = true;
    const selAcc = this.aService.selected.getValue();
    const from = selAcc.name;
    const to = this.sendForm.get('to').value.toLowerCase();
    const amount = parseFloat(this.sendForm.get('amount').value);
    let memo = this.sendForm.get('memo').value;
	if (memo == '') {
		memo = 'Перевод';
	}
    const publicKey = this.aService.getPublicKey(selAcc.details['permissions'], 'active');
    if (amount > 0 && this.sendForm.valid) {
      this.crypto.authenticate(this.confirmForm.get('pass').value, publicKey).then((res) => {
        // console.log(res);
        if (res) {
          let contract = 'dars.invoice';
          const tk_name = this.sendForm.get('token').value;
          let precision = 8;
		  if (tk_name === 'DUSD') {
		   precision = 2;
		  }
		  let invoice_id = Math.round(Math.random() * (9999999999 - 1000000000) + 1000000000);
		  
		  this.dars.addAccountPermission(from, 'dars.invoice');

	      const invoice_tr = this.dars.dars.transaction({
		   actions: [{
		    account: 'dars.invoice',
			name: 'add',
			authorization: [{
			 actor: from,
			 permission: 'active'
			}],
			data: {
			 from:     from,
			 to:       to,
			 quantity: amount.toFixed(precision) + " " + tk_name,
			 memo:     "transferTokens;id=" + invoice_id + ";",
			 comment:  memo
			}
		   }],
	      }).then((invoice_result) => {
		   if (invoice_result.broadcast === true) {
            setTimeout(() => {
			 let pkey = -1;	
				
			 this.dars.getInvoices(from).then((invoices) => {
			  if (invoices.rows.length > 0) {
			   for (let idx=0; idx<invoices.rows.length; idx++) {
				if ((pkey == -1) &&   
			        (invoices.rows[idx]["from"] == from) &&
				    (invoices.rows[idx]["payed"] == 0)) {
				 let arr = invoices.rows[idx]["memo"].split(";");
				 if (arr[0] == "transferTokens") {
				  for (let i=1; i<arr.length; i++) {
				   let arr2 = arr[i].split("=");
				   if ((arr2[0] == "id") &&
					   (arr2[1] == invoice_id)) {
				    pkey = invoices.rows[idx]["pkey"];
					break;
				   }
			      }
				 }
			    }
			   } //for invoices
			  } //if (invoices.rows.length > 0)

			  if (pkey >= 0) {
	           const pay_tr = this.dars.dars.transaction({
		        actions: [{
		         account: 'dars.invoice',
			     name: 'transfer',
			     authorization: [{
			      actor: from,
			      permission: 'active'
			     }],
			     data: {
			      from:     from,
			      to:       to,
			      quantity: amount.toFixed(precision) + " " + tk_name,
			      memo:     pkey + ";transferTokens;id=" + invoice_id + ";"
			     }
		        }],
	           }).then((pay_result) => {
			    if (pay_result.broadcast === true) {	 
			     this.wrongpass = '';
                 this.sendModal = false;
                 this.busy = false;
                 this.showToast('успешно', 'Транзакция отправлена', 'Информация о транзакциях отображается в разделе История.');

                 this.aService.refreshFromChain();
                 setTimeout(() => {
                  const sel = this.aService.selected.getValue();
                  this.unstaked = sel.drs_balance - sel.staked - sel.unstaking;
			      this.DRSBalance = sel.drs_balance;
			      this.DUSDBalance = sel.dusd_balance;
                 }, 2000);

                 this.confirmForm.reset();
                 if (this.add === true && this.sendForm.get('alias').value !== '') {
                  this.addContactOnSend();
			     }
			    } else { //if (pay_result.broadcast === true)
                 this.wrongpass = JSON.parse(pay_result).error.details[0].message;
                 this.busy = false;
			    }
			   }).catch((error) => { //then pay_tr.pay_result
                console.log('CatchPay', error);
                this.wrongpass = 'Ошибка отправки!';
                this.busy = false;
               });
			  } else { //if (pkey >= 0)
               console.log('pkey == -1');
               this.wrongpass = 'Ошибка отправки!';
               this.busy = false;
			  }
             }).catch((error) => { //then.invoices
	          console.log("Ошибка получения списка инвойсов:", error);
              this.wrongpass = 'Ошибка отправки!';
              this.busy = false;
	         });
			}, 2000); //invoice_result.setTimeout(()
           } else { //if (invoice.broadcast === true)
            this.wrongpass = JSON.parse(invoice_result).error.details[0].message;
            this.busy = false;
           }
          }).catch((error) => { //then invoice_tr.result
           console.log('CatchInvoice', error);
           this.wrongpass = 'Ошибка отправки!';
           this.busy = false;
          });
        } else { //if (res)
          this.busy = false;
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => { //this.crypto.authenticate
        console.log(err);
        this.busy = false;
        this.wrongpass = 'Неверный пароль!';
      });
    }
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

  openEditContactModal(contact) {
    console.log(contact);
    this.contactForm.patchValue({
      account: contact.account
    });
    this.editContactModal = true;
    this.selectedEditContact = contact;
  }

  doEditContact() {
    const index = this.contacts.findIndex((el) => {
      return el.account === this.selectedEditContact.account;
    });
    this.contacts[index].name = this.contactForm.get('name').value;
    this.editContactModal = false;
    this.selectedEditContact = null;
    this.contactForm.reset();
    this.addDividers();
    this.storeContacts();
  }

  openDeleteContactModal(contact) {
    this.deleteContactModal = true;
    this.selectedEditContact = contact;
  }

  doDeleteContact() {
    const index = this.contacts.findIndex((el) => {
      return el.account === this.selectedEditContact.account;
    });
    this.contacts.splice(index, 1);
    this.deleteContactModal = false;
    this.addDividers();
    this.storeContacts();
  }

}
