import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {HttpClient} from '@angular/common/http';

import * as moment from 'moment';

//.............................................................................................	  
import * as DARSJS from '../../../assets/eos.js';
//.............................................................................................	  

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit, AfterViewInit {
  echartsInstance: any;
//  location: string[];
//  country: string[];
//  graphMerge: any;
  options: any;

  initOptions = {
    renderer: 'z',
    width: 1000,
    height: 400
  };
  
  //map
  data: any[];
  updateOptions: any;
  
  moment: any;
  producers = [];
  totalProducers: number;
  DRSBalance: number;
  loadingProducers: boolean;
  
  @ViewChild('producersTable') public producersTable: DataTable;
  
  config: ToasterConfig;
  
  confirmForm:  	   FormGroup;
  fromAccount:  	   string;
  voteProducerAccount: string;
  toAccount:    	   string;
  sendQuantity: 	   string;
  minRefundDate: 	   string;
  paymentMemo:  	   string;
  paymentComment:  	   string;
  wrongpass:    	   string;
  amounterror:  	   string;
  numberMask = createNumberMask({
    prefix: '',
    allowDecimal: true,
    includeThousandsSeparator: false,
    decimalLimit: 8,
  });
  
  busy: boolean;
  sendModal: boolean;

  constructor(private http: HttpClient,
              public  aService: AccountsService,
              public  dars: DARSJSService,
              public  crypto: CryptoService,
              private fb: FormBuilder,
              private toaster: ToasterService) {
    this.moment = moment;
    this.producers = [];
	this.totalProducers = 0;
    this.loadingProducers = true;
	
	this.confirmForm = this.fb.group({
	  refunddate: [''],
	  amount: ['', Validators.required],
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.busy = false;
	this.sendModal = false;
				  
    this.options = {
      geo: {
        map: 'world',
        roam: false,
        left: 0,
        right: 0,
        silent: true,
        aspectScale: 1,
        itemStyle: {
          normal: {
            borderColor: '#1076a1',
            color: '#17181c'
          }
        }
      },
      tooltip: {
		show: false,
		trigger: 'item',
        formatter: '{b}'
	  },
      animationDuration: 1500,
      animationEasingUpdate: 'quinticInOut',
      series: [
        {
          type: 'graph',
          coordinateSystem: 'geo',
          symbol: 'pin',
          symbolSize: 15,
          data: this.data,
          animation: true,
          animationDuration: 2000,
          focusNodeAdjacency: true,
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1,
              shadowBlur: 10,
              color: '#fff',
              shadowColor: 'rgba(0, 0, 0, 0.3)'
            }
          },
          label: {
            position: 'top',
            formatter: '{b}',
            show: false,
            distance: 6,
            fontSize: 16
          },
          lineStyle: {
            color: 'source',
            curveness: 0.01,
            width: 2
          },
          force: {
            repulsion: 600,
            edgeLength: 150,
          },
          emphasis: {
            lineStyle: {
              width: 10
            }
          }
        }
      ]
    };
  }

  extOpen(value) {
    window['shell'].openExternal(value);
  }

  ngOnInit() {
  }
  
  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
		  this.DRSBalance = sel.drs_balance;
          this.reloadProducers();
          this.aService.refreshFromChain();
        }, 50);
      }
    });
  }

  reloadProducers() {
    this.dars.getProducers().then((producers) => {
     if (producers.rows.length > 0) {
		this.producers = [];
		this.data = [];
		producers.rows.forEach((line) => {
			let social = [];
				let arr = line["social"].split(";");
				for (let i=0; i<arr.length; i++) {
					let arr2 = arr[i].split(":");
					social[arr2[0]] = arr2[1];
				}
			let geo = [];
				let arr3 = line["geo_location"].split(";");
				for (let i=0; i<arr3.length; i++) {
					let arr4 = arr3[i].split(":");
					geo[arr4[0]] = arr4[1];
				}
			
			const obj = {
				position:  		this.producers.length+1,
				name: 			line["name"],
				account:  		line["owner"],
				lat:			geo["lat"],
				lon:			geo["lon"],
				place:			geo["place"],
				website:    	line["site_url"],
				social: 		social,
				total_votes:	Math.round(1000*line["total_votes"])/10
			};
			this.producers.push(obj);
			this.addPin(obj);
		});
		this.totalProducers = this.producers.length;
      } else {
		this.producers = [];
		this.data = [];
		this.totalProducers = 0;
	  }
	 this.loadingProducers = false;
	 this.producersTable.reset();
    }).catch((err) => {
	 this.loadingProducers = false;
	});
  }

  addPin(bp) {
	const account = bp['account'];
    const lat = bp['lat'];
    const lon = bp['lon'];
    if ((lon < 180 && lon > -180) && (lat < 90 && lat > -90)) {
		if (bp['position'] <= 1) { //21
			this.data.push({
				name: account,
				symbol: 'diamond',
				symbolSize: 10,
				itemStyle: {
					color: '#6cff46',
					borderWidth: 0
				},
				value: [lon, lat]
			});
		} else {
			this.data.push({
				name: account,
				symbol: 'circle',
				symbolSize: 8,
				itemStyle: {
					color: '#feff4b',
					borderWidth: 0
				},
				value: [lon, lat]
            });		
		}
	}
    this.updateOptions = {
		series: [{
			data: this.data
		}]
    }
  }
  
  doVote(producer) {
	this.confirmForm.reset();
	this.fromAccount = this.aService.selected.getValue().name;
	this.toAccount = "dars.rent";
	this.sendQuantity = "0.00000000";
	this.paymentMemo = "";
	this.paymentComment = "Голосование за блокпродюсера " + producer;
	this.amounterror = "";
	this.wrongpass = "";
	this.confirmForm.controls['refunddate'].setValue(this.moment().add(1, 'M').format('YYYY-MM-DD'));
	this.minRefundDate = this.moment().add(1, 'd').format('YYYY-MM-DD');
	this.voteProducerAccount = producer;
	this.sendModal = true;
  }
   
  checkAmount() {
    if (parseFloat(this.confirmForm.get('amount').value) === 0 || 
	   this.confirmForm.get('amount').value === '') {
      this.confirmForm.controls['amount'].setErrors({'incorrect': true});
      this.amounterror = 'неверное количество';
    } else {
      this.confirmForm.controls['amount'].setErrors(null);
      this.amounterror = '';
    }
  }

  transfer() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.confirmForm.get('pass').value, publicKey).then((res) => {
        if (res) {
		  this.dars.addAccountPermission(this.fromAccount, 'dars.rent');
			
		  const amount = parseFloat(this.confirmForm.get('amount').value);
		  let precision = 8;
		  const refundts = this.moment(this.confirmForm.get('refunddate').value).format("X");
		  
			if (amount > this.DRSBalance) {
				this.wrongpass = 'Указанная сумма больше, чем количество токенов на Вашем балансе!';
			} else {
				if (refundts > this.moment().format("X")) {
					this.busy = true;
					const tr = this.dars.dars.transaction({
						actions: [{
							account: 'dars.rent',
							name: 'vote',
							authorization: [{
								actor: this.aService.selected.getValue().name,
								permission: 'active'
							}],
							data: {
								from:     this.aService.selected.getValue().name,
								amount:   amount.toFixed(precision) + ' DRS',
								refundts: refundts,
								producer: this.voteProducerAccount
							}
						}]
					}).then((result) => {
						if (result.broadcast === true) {
							this.busy = false;
							this.wrongpass = '';
							this.sendModal = false;
							this.showToast('успешно', 'Транзакция отправлена', 'Информация о транзакциях отображается в разделе История.');
							this.aService.refreshFromChain();
							this.reloadProducers();
						} else {
							this.busy = false;
							this.wrongpass = JSON.parse(result).error.details[0].message;
						}
					}).catch((error) => {
						this.busy = false;
						this.wrongpass = 'Ошибка отправки!';
						console.log('Catch2', error);
					});
				} else {
					this.wrongpass = 'Укажите верную дату окончания!';
				}
			} //amount > this.DRSBalance
        } else {
         this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
       this.wrongpass = 'Неверный пароль!';
       console.log(err);
      });
  }
   
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

  onChartInit(e: any) {
    this.echartsInstance = e;
  }

}
