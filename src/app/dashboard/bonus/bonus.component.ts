import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';

import * as moment from 'moment';

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.css']
})

export class BonusComponent implements OnInit, AfterViewInit, OnDestroy {
  moment: any;
  bonuses = [];
  totalBonuses: number;
  loading: boolean;
  
  @ViewChild('bonusTable') public bonusTable: DataTable;

  config: ToasterConfig;
  
  constructor(private fb: FormBuilder,
              public aService: AccountsService, 
			  public dars: DARSJSService,
			  private crypto: CryptoService,
			  private toaster: ToasterService) {
    this.moment = moment;
    this.bonuses = [];
	this.totalBonuses = 0;
    this.loading = true;
 }

  ngOnInit() {
 }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
          this.reloadBonuses(sel.name);
          this.aService.refreshFromChain();
        }, 50);
      }
    });
  }

  refresh() {
    this.reloadBonuses(this.aService.selected.getValue().name);
  }
  
  reloadBonuses(account) {
    this.dars.getBonuses(account).then((bonuses) => {
     if (bonuses.rows.length > 0) {
		this.bonuses = [];
		bonuses.rows.forEach((bonuse) => {
			if (bonuse["to"] == account) {
				let date = this.moment.unix(bonuse["ts"]).format("YYYY-MM-DD HH:mm:ss");
				const obj = {
					id:  	  bonuse["pkey"],
					date:  	  date,
					to:    	  bonuse["to"],
					amount:   bonuse["amount"],
					comment:  bonuse["comment"]
				};
				this.bonuses.unshift(obj);
			}
		});
		this.totalBonuses = this.bonuses.length;
      } else {
		console.log("Пустой список бонусов");
		this.bonuses = [];
		this.totalBonuses = 0;
	  }
	 this.loading = false;
	 this.bonusTable.reset();
    }).catch((err) => {
	 console.log("Ошибка получения списка бонусов:", err);
	 this.loading = false;
	});
  }
  
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

}
