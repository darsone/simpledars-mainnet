import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';

import * as moment from 'moment';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})

export class HistoryComponent implements OnInit, AfterViewInit, OnDestroy {
  moment: any;
  histories = [];
  totalHistories: number;
  loading: boolean;
  
  @ViewChild('historyTable') public historyTable: DataTable;

  config: ToasterConfig;
  
  constructor(private fb: FormBuilder,
              public aService: AccountsService, 
			  public dars: DARSJSService,
			  private crypto: CryptoService,
			  private toaster: ToasterService) {
    this.moment = moment;
    this.histories = [];
    this.totalHistories = 0;
    this.loading = true;
 }

  ngOnInit() {
 }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
          this.reloadHistories(sel.name, '');
          this.aService.refreshFromChain();
        }, 50);
      }
    });
  }

  refresh(token) {
    this.reloadHistories(this.aService.selected.getValue().name, token);
  }
  
  reloadHistories(account, token) {
    this.dars.getHistoriesFrom(account).then((histories) => {
     this.histories = [];
	 
	 let from_histories = [];
     if (histories.rows.length > 0) {
		histories.rows.forEach((historie) => {
			if (historie["from"] == account) {
				let needadd = true;
				let date = this.moment.unix(historie["timestamp"]).format("YYYY-MM-DD HH:mm:ss");
				let amount = "-" + historie["quantity"];
				let comission = "";
				let balance = historie["frombalance"];
				let comment = historie["memo"];

				if ((historie["to"] == "dars.invoice")||
					(historie["to"] == "dars.wh")) {
				 let arr = comment.split(";");
				 if (arr[1] == "createCompany") {
				  let firmname = "";
				  for (let i=2; i<arr.length; i++) {
				   let arr2 = arr[i].split("=");
				   if (arr2[0] == "alias") {
					firmname = arr2[1];
				   }
				  }
				  comment = "Регистрация компании " + firmname;
				 } else
				 if (arr[1] == "buyPackage") {
				  let packagename = "";
				  for (let i=2; i<arr.length; i++) {
				   let arr2 = arr[i].split("=");
				   if (arr2[0] == "package") {
					packagename = arr2[1];
				   }
				  }
				  comment = "Покупка пакета " + packagename;
				 } else
				 if (arr[1] == "payFee") {
				  comment = "Комиссия за операцию";
				  comission = amount;
				  amount = "";
				 } else {
				  comment = "";
				 }
				}
				if (comment == "") {
					comment = "Перевод";
				}
				
				if (token != '') {
				 if (historie["quantity"].split(' ')[1] != token) {
				  needadd = false;
				 }					 
				}
				
				if (needadd) {
				 const obj = {
					id:  	  	historie["pkey"],
					date:  	  	date,
					from:     	historie["from"],
					to:    	  	historie["to"],
					amount:   	amount,
					comission:  comission,
					color:    	"red",
					comment:  	comment,
					balance:  	balance
				 };
				 from_histories.unshift(obj);
				}
			}
		});
		
		//Объединяем строчки комиссии со строчками операций
		for (let i=0; i<from_histories.length; i++) {
			if ((from_histories[i]["comission"] != "") &&
				(from_histories[i]["amount"] == "")) {
				for (let k=0; k<from_histories.length; k++) {
					if ((k != i) &&
						(from_histories[k]["comission"] == "") &&
						(from_histories[k]["amount"] != "") &&
						(from_histories[k]["date"] == from_histories[i]["date"])) {
							from_histories[k]["comission"] = from_histories[i]["comission"];
							from_histories[i]["comission"] = "";
							break;
					}
				}
			}
		}
		
		//Сохраняем строчки истории
		for (let i=0; i<from_histories.length; i++) {
			if ((from_histories[i]["comission"] != "") ||
				(from_histories[i]["amount"] != "")) {
					this.histories.unshift(from_histories[i]);
			}
		}
      } else {
		console.log("Пустой список истории (from)");
	  }
	  
      this.dars.getHistoriesTo(account).then((histories) => {
       if (histories.rows.length > 0) {
		histories.rows.forEach((historie) => {
			if (historie["to"] == account) {
				let needadd = true;
				let date = this.moment.unix(historie["timestamp"]).format("YYYY-MM-DD HH:mm:ss");
				let balance = historie["tobalance"];
				let comment = historie["memo"];
				if (comment == "") {
					comment = "Перевод";
				}
				if (token != '') {
				 if (historie["quantity"].split(' ')[1] != token) {
				  needadd = false;
				 }					 
				}
				if (needadd) {
				 const obj = {
					id:  	  	historie["pkey"],
					date:  	  	date,
					from:     	historie["from"],
					to:    	  	historie["to"],
					amount:   	historie["quantity"],
					comission:  "",
					color:    	"green",
					comment:  	comment,
					balance:  	balance
				 };
				 this.histories.unshift(obj);
				}
			}
		});
       } else {
		console.log("Пустой список истории (to)");
	   }

	  this.histories.sort(function(a, b) {
		var valueA, valueB;
		valueA = a["date"];
		valueB = b["date"];
		if (valueA < valueB) {
        	    return 1;
		}
	    else if (valueA > valueB) {
         return -1;
        }
        return 0;
       });
	   
		this.totalHistories = this.histories.length;
		this.loading = false;
        this.historyTable.reset();
     }).catch((err) => {
	  console.log("Ошибка получения списка истории (to):", err);
	 });
	}).catch((err) => {
	 console.log("Ошибка получения списка истории (from):", err);
	 this.loading = false;
	});
  }
  
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

}
