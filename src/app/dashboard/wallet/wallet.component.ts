import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';

import * as moment from 'moment';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})

export class WalletComponent implements OnInit, AfterViewInit, OnDestroy {
  moment: any;
  invoices = [];
  totalInvoices: number;
  DRSBalance: number;
  DUSDBalance: number;
  hasNewInvoices: boolean;
  loading: boolean;
  
  @ViewChild('invoiceTable') public invoiceTable: DataTable;

  config: ToasterConfig;
  
  confirmForm:  FormGroup;
  paymentId:    number;
  fromAccount:  string;
  toAccount:    string;
  sendQuantity: string;
  paymentMemo:  string;
  paymentComment: string;
  needRent:     boolean;
  wrongpass:    string;
  busy:         boolean;
  sendModal:    boolean;

  constructor(private fb: FormBuilder,
              public aService: AccountsService, 
	      public dars: DARSJSService,
	      private crypto: CryptoService,
	      private toaster: ToasterService) {
    this.moment = moment;
    this.invoices = [];
    this.totalInvoices = 0;
    this.hasNewInvoices = false;
    this.loading = true;
	
    this.confirmForm = this.fb.group({
      dorent: [''],
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.busy = false;
	this.sendModal = false;
  }

  ngOnInit() {
 }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
          this.reloadInvoices(sel.name);
          this.aService.refreshFromChain();
          this.DRSBalance = sel.drs_balance;
          this.DUSDBalance = sel.dusd_balance;
        }, 50);
      }
    });
  }

  refresh() {
    this.reloadInvoices(this.aService.selected.getValue().name);
  }
  
  reloadInvoices(account) {
    this.dars.getInvoices(account).then((invoices) => {
     if (invoices.rows.length > 0) {
		this.hasNewInvoices = false;
		this.invoices = [];
		invoices.rows.forEach((invoice) => {
			if (invoice["from"] == account) {
				if (invoice["payed"] == 0) {
					this.hasNewInvoices = true;
				}
				let date = this.moment.unix(invoice["timestamp"]).format("YYYY-MM-DD HH:mm:ss");
				const obj = {
					id:  	  invoice["pkey"],
					payed: 	  invoice["payed"],
					date:  	  date,
					from:  	  invoice["from"],
					to:    	  invoice["to"],
					quantity: invoice["quantity"],
					memo: 	  invoice["memo"],
					comment:  invoice["comment"]
				};
				this.invoices.unshift(obj);
			}
		});
		this.totalInvoices = this.invoices.length;
      } else {
		console.log("Пустой список инвойсов");
		this.invoices = [];
		this.totalInvoices = 0;
		this.hasNewInvoices = false;
	  }
	 this.loading = false;
	 this.invoiceTable.reset();
    }).catch((err) => {
	 console.log("Ошибка получения списка инвойсов:", err);
	 this.loading = false;
	});
  }
  
  parseDRS(tk_string) {
    if (tk_string.split(' ')[1] === 'DRS') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }
  
  doPayment(id) {
	this.invoices.forEach((invoice) => {
		if (invoice["id"] == id) {
			this.confirmForm.reset();
			this.paymentId = id;
			this.fromAccount = invoice["from"];
			this.toAccount = invoice["to"];
			this.sendQuantity = invoice["quantity"];
			if (this.parseDRS(invoice["quantity"]) > this.DRSBalance) {
			 this.needRent = true;
			} else {
			 this.needRent = false;
			}
			this.paymentMemo = invoice["memo"];
			this.paymentComment = invoice["comment"];
			this.wrongpass = "";
			this.sendModal = true;
		}
	});
  }

  transfer() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.confirmForm.get('pass').value, publicKey).then((res) => {
		// console.log(res);
        if (res) {
		  if ((this.needRent) && (this.DUSDBalance < 0.01)) {
			this.wrongpass = 'На Вашем счету нет DUSD';
		  } else {
			this.dars.addAccountPermission(this.fromAccount, 'dars.invoice');
			
			this.busy = true;
			this.dars.transfer("dars.invoice", this.fromAccount, this.toAccount, this.sendQuantity, this.paymentId+";"+this.paymentMemo).then((result) => {
				if (result === true) {
					this.busy = false;
					this.wrongpass = '';
					this.sendModal = false;
					this.showToast('успешно', 'Транзакция отправлена', 'Информация о транзакциях отображается в разделе История.');
					this.aService.refreshFromChain();
					this.reloadInvoices(this.fromAccount);
				} else {
					this.busy = false;
					this.wrongpass = JSON.parse(result).error.details[0].message;
				}
			}).catch((error) => {
				this.busy = false;
				console.log('Catch2', error);
				if (error.error.code === 3081001) {
					this.wrongpass = 'Недостаточно средств для выполнения транзакции';
				} else {
					this.wrongpass = 'Ошибка отправки!';
				}
			});
		  }
        } else {
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
        console.log(err);
        this.wrongpass = 'Неверный пароль!';
      });
  }
  
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

}
