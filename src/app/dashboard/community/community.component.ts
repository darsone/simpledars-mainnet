import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {ClrWizard, ClrWizardPage} from '@clr/angular';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';

import * as moment from 'moment';

//.............................................................................................	  
import * as DARSJS from '../../../assets/eos.js';
//.............................................................................................	  

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})

export class CommunityComponent implements OnInit, AfterViewInit, OnDestroy {
  moment: any;
  votingtypes = [];
  votings = [];
  fields = [];
  values = [];
  totalVotings:   number;
  loadingVotings: boolean;
  
  votingID:       number;
  voteResult:     number;
  voteResultStr:  string;
  votingDetail = [];
  selectedVotingType: number;
  
  @ViewChild('votingsTable') public votingsTable: DataTable;

  @ViewChild('createVotingWizard') createVotingWizard: ClrWizard;
  @ViewChild('createFieldsPage') createFieldsPage: ClrWizardPage;
  
  config: ToasterConfig;
  
  createForm: 	FormGroup;
  editForm:     FormGroup;
  deleteForm:   FormGroup;
  voteForm:     FormGroup;
  wrongpass:    string;
  wrongvoting:	string;

  busy: 		boolean;
  loadedFields: boolean;
  createModal:  boolean;
  editModal:    boolean;
  deleteModal:  boolean;
  voteModal:    boolean;
  detailModal:  boolean;

  constructor(private fb: FormBuilder,
              public aService: AccountsService, 
			  public dars: DARSJSService,
			  private crypto: CryptoService,
			  private toaster: ToasterService) {
    this.moment = moment;
    this.votings = [];
	this.totalVotings = 0;
    this.loadingVotings = true;
	
	this.createForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(10)]],
	  votingcomment: ['', []]
    });
	this.editForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(10)]],
	  votingcomment: ['', []]
    });
	this.deleteForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.voteForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.busy 		 = false;
	this.createModal = false;
	this.editModal   = false;
	this.deleteModal = false;
	this.voteModal   = false;
	this.detailModal = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
		  this.reloadVotingTypes();
          this.aService.refreshFromChain();
        }, 50);
      }
    });
  }

  reloadVotingTypes() {
    this.dars.getVotingTypes().then((votingtypes) => {
     if (votingtypes.rows.length > 0) {
		this.votingtypes = [];
		votingtypes.rows.forEach((line) => {
			if ((line["status"] & 1) == 1) {
				let scheme = "";
				if (line["scheme"] == 0) {
					scheme = "Необходимо набрать " + line["quorum"] + "% голосов ЗА";
				} else
				if (line["scheme"] == 1) {
					scheme = "Необходимо набрать 50% + 1 голос ЗА";
				} else
				if (line["scheme"] == 2) {
					scheme = "Необходимо набрать 2/3 голосов ЗА";
				} else
				if (line["scheme"] == 3) {
					scheme = "Необходимо набрать " + line["quorum"] + " голоса(ов) ЗА";
				}
				
				const obj = {
					id:  	  	line["pkey"],
					name:  	  	line["comment"],
					status:  	line["status"],
					voter:  	line["voters"],
					scheme:  	scheme,
					duration:   line["duration"],
					quorum:     line["quorum"]
				};
				this.votingtypes.unshift(obj);
			}
		});
		this.reloadVotings();
      } else {
		this.votingtypes = [];
	  }
    }).catch((err) => {
	});
  }
  
  reloadVotings() {
	this.votings = [];
	let processed = 0;
	
    this.dars.getVotings().then((votings) => {
		if (votings.rows.length > 0) {			
			for (let index=0; index<votings.rows.length; index++) {
				let line = votings.rows[index];
				let foraccount = true;	//Голосование предназначено для данного аккаунта
			
				let audited = true;
				if ((line["status"] & 8) == 8)
					audited = false;

				let finished = false;
				if ((line["status"] & 16) == 16)
					finished = true;

				let begindate = this.moment.unix(line["begints"]).format("YYYY-MM-DD");
				let enddate = this.moment.unix(line["endts"]).format("YYYY-MM-DD");
				let inperiod = false;
				if ((this.moment().format('X') >= line["begints"]) &&
					(this.moment().format('X') <= line["endts"]))
					inperiod = true;
				
				let typename = "";
				let scheme = "";	
				for (let i=0; i<this.votingtypes.length; i++)
					if (this.votingtypes[i]["id"] == line["votingtypeid"]) {
						typename = this.votingtypes[i]["name"];
						scheme = this.votingtypes[i]["scheme"];
						break;
					}
					
				let voted = 0;
				this.dars.getVotingVotes(line["pkey"]).then((votes) => {
					processed++;
					
					for (let i=0; i<votes.rows.length; i++)
						if (votes.rows[i]["owner"] == this.aService.selected.getValue().name) {
							voted = votes.rows[i]["result"];
							break;
						}

					const obj = {
						id:  	  	line["pkey"],
						typeid:  	line["votingtypeid"],
						typename:   typename,
						audited:	audited,
						finished:	finished,
						begindate:  begindate,
						enddate:    enddate,
						inperiod:	inperiod,
						scheme:		scheme,
						owner:  	line["owner"],
						comment:  	line["comment"],
						approve:	line["approve"],
						reject:		line["reject"],
						voted:      voted
					};
					
					if (((line["status"] & 1) == 1)&&
						(foraccount)) {
						this.votings.unshift(obj);
					}
						
					//Асинхронный вызов	!
					if (processed == votings.rows.length) {
						this.votings.sort(function(a, b) {
							if (a["begindate"] < b["begindate"]) {
								return 1;
							} else 
							if (a["begindate"] > b["begindate"]) {
								return -1;
							} else {
								if (a["id"] < b["id"]) {
									return 1;
								} else 
								if (a["id"] > b["id"]) {
									return -1;
								} else 
									return 0;
							}
						});

						this.totalVotings = this.votings.length;
						this.loadingVotings = false;
						this.votingsTable.reset();
					}
				}).catch((error) => {
					this.loadingVotings = false;
				});
			} //for
		} else {
			this.loadingVotings = false;
		}
	}).catch((err) => {
		this.loadingVotings = false;
	});
  }

  detailVoting(votingid) {
	for (let i=0; i<this.votings.length; i++)
		if (this.votings[i]["id"] == votingid) {
			this.votingDetail = this.votings[i]; 
			break;
		}
		
	this.values = [];  
	this.dars.getVotingValues(votingid).then((values) => {
		values.rows.forEach((line) => {
			const obj = {
				id:  	  	line["pkey"],
				votingid:  	line["votingid"],
				fieldid:  	line["fieldid"],
				value:  	line["value"]
			};
			this.values.unshift(obj);
		});
	   
		this.dars.getVotingFields(this.votingDetail["typeid"]).then((fields) => {
			this.fields = [];
			fields.rows.forEach((line) => {
				let value = line["defaultvalue"];
				for (let i=0; i<this.values.length; i++)
					if (this.values[i]["fieldid"] == line["pkey"]) {
						value = this.values[i]["value"];
						break;
					}
				const obj = {
					id:  	  	line["pkey"],
					visible:  	line["visible"],
					num:   		line["num"],
					required:   line["required"],
					name:    	line["name"],
					comment:  	line["comment"],
					fieldtype:  line["fieldtype"],
					value:  	value
				};
				this.fields.unshift(obj);
			});
		
			this.fields.sort(function(a, b) {
				var valueA, valueB;
				valueA = a["num"];
				valueB = b["num"];
				if (valueA > valueB) {
					return 1;
				}
				else if (valueA < valueB) {
					return -1;
				}
				return 0;
			});
			this.detailModal = true;
		}).catch((err) => {
		});
    }).catch((err) => {
	});
  }

  createVoting() {
	this.selectedVotingType = 0;
	this.createVotingWizard.reset();
	this.createForm.reset();
	this.wrongpass = "";
	this.wrongvoting = "";
	this.loadedFields = false;
	this.createModal = true;
  }
  
  doCreateCancel() {
	this.createVotingWizard.reset();
	this.createVotingWizard.close();
  }
  
  votingTypeChange() {
	this.wrongvoting = "";
    this.dars.getVotings().then((votings) => {
     for (let i = 0; i<votings.rows.length; i++) {
		if ((votings.rows[i]["owner"] == this.aService.selected.getValue().name) &&
			(votings.rows[i]["votingtypeid"] == this.selectedVotingType) &&
			((votings.rows[i]["status"] & 1) == 1)) {
			this.wrongvoting = "Такое голосование уже существует!";
			break;
		 }
	 }
    }).catch((err) => {
	});
  }

  loadFieldsForCreate() {
	let typeid = this.selectedVotingType;
	this.votingDetail["owner"] = this.aService.selected.getValue().name;
	this.votingDetail["typeid"] = typeid;
	this.votingDetail["begindate"] = this.moment().format("YYYY-MM-DD"); 
	for (let i=0; i<this.votingtypes.length; i++)
		if (this.votingtypes[i]["id"] == typeid) {
			this.votingDetail["enddate"]  = this.moment().add(this.votingtypes[i]["duration"], 'd').format("YYYY-MM-DD");;  
			this.votingDetail["typename"] = this.votingtypes[i]["name"];
			this.votingDetail["scheme"] = this.votingtypes[i]["scheme"];
			break;			
		}
	this.votingDetail["comment"] = "";
	
	let existent_fields = [];
	for (const fieldname in this.createForm.controls)
		existent_fields.unshift(fieldname);
//console.log("existent_fields=", existent_fields);
	for (let i=0; i < existent_fields.length; i++) {
		if (existent_fields[i].indexOf("field_") === 0) {
			this.createForm.removeControl(existent_fields[i]);
		}
	}
//console.log("controls=", this.createForm.controls);
	  
	this.dars.getVotingFields(typeid).then((fields) => {
		this.fields = [];
		fields.rows.forEach((line) => {
			const obj = {
				id:  	  	line["pkey"],
				visible:  	line["visible"],
				num:   		line["num"],
				required:   line["required"],
				name:    	line["name"],
				comment:  	line["comment"],
				fieldtype:  line["fieldtype"],
				value:  	line["defaultvalue"]
			};
			if (obj["fieldtype"] == 2) //2 - публичный ключ
				obj["value"] = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
						
			if (obj["fieldtype"] == 0) { //0 - строка
				if (obj["required"]) {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required])));
				} else {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([])));
				}
			} else
            if (obj["fieldtype"] == 1) { //1 - имя аккаунта
				if (obj["required"]) {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required, Validators.minLength(12)])));
				} else {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.minLength(12)])));
				}
			} else
            if (obj["fieldtype"] == 2) { //2 - публичный ключ
				if (obj["required"]) {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required])));
				} else {
					this.createForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([])));
				}
			} else
            if (obj["fieldtype"] == 3) { //3 - p2p-адрес
				obj["value_ip"] = "";
				obj["value_port"] = "";
				if (obj["required"]) {
					this.createForm.addControl("field_"+obj["name"]+"_ip",   this.fb.control(obj["value_ip"], Validators.compose([Validators.required])));
					this.createForm.addControl("field_"+obj["name"]+"_port", this.fb.control(obj["value_port"], Validators.compose([Validators.required])));
				} else {
					this.createForm.addControl("field_"+obj["name"]+"_ip",   this.fb.control(obj["value_ip"], Validators.compose([])));
					this.createForm.addControl("field_"+obj["name"]+"_port", this.fb.control(obj["value_port"], Validators.compose([])));
				}
			} else
            if (obj["fieldtype"] == 4) { //4 - георасположение
				obj["value_lat"] = "";
				obj["value_lon"] = "";
				obj["value_place"] = "";
				if (obj["required"]) {
					this.createForm.addControl("field_"+obj["name"]+"_lat",   this.fb.control(obj["value_lat"], Validators.compose([Validators.required])));
					this.createForm.addControl("field_"+obj["name"]+"_lon",   this.fb.control(obj["value_lon"], Validators.compose([Validators.required])));
					this.createForm.addControl("field_"+obj["name"]+"_place", this.fb.control(obj["value_place"], Validators.compose([Validators.required])));
				} else {
					this.createForm.addControl("field_"+obj["name"]+"_lat",   this.fb.control(obj["value_lat"], Validators.compose([])));
					this.createForm.addControl("field_"+obj["name"]+"_lon",   this.fb.control(obj["value_lon"], Validators.compose([])));
					this.createForm.addControl("field_"+obj["name"]+"_place", this.fb.control(obj["value_place"], Validators.compose([])));
				}
			} else
            if (obj["fieldtype"] == 111) {  //111 - файл
			}

			this.fields.unshift(obj);
		});
		
		this.fields.sort(function(a, b) {
			var valueA, valueB;
			valueA = a["num"];
			valueB = b["num"];
			if (valueA > valueB) {
				return 1;
			}
			else if (valueA < valueB) {
				return -1;
			}
			return 0;
		});
		this.loadedFields = true;
		this.createVotingWizard.next();
	}).catch((err) => {
	});
  }
  
  sendCreatedVoting() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.createForm.get('pass').value, publicKey).then((res) => {
//console.log("crypto.authenticate.res=",res);		
        if (res) {
			this.dars.addAccountPermission(this.aService.selected.getValue().name, 'dars.compose');
			
			this.busy = true;
			//Создаем голосование
			const tr = this.dars.dars.transaction({
				actions: [{
					account: 'dars.compose',
					name: 'addvoting',
					authorization: [{
						actor: this.aService.selected.getValue().name,
						permission: 'active'
					}],
					data: {
						votingtypeid:   this.selectedVotingType,
						status:			1,
						begints:		this.moment().format('X'),
						owner:   		this.aService.selected.getValue().name,
						purpose:		"dars",									//Изменить для голосования за фирму !!!
						comment:		this.createForm.get('votingcomment').value,
						pkey:			0
					}
				}]
			}).then((result) => {
//console.log("addvoting.result=",result);		
				if (result.broadcast === true) {
					setTimeout(() => {
						//Ищем идентификатор голосования (только 1 активное голосование такого типа может иметь данный владелец)
						let votingID = 0;
						this.dars.getVotings().then((votings) => {
							for (let i=0; i<votings.rows.length; i++) {
								if ((votings.rows[i]["owner"] == this.aService.selected.getValue().name) &&
									(votings.rows[i]["votingtypeid"] == this.selectedVotingType) &&
									((votings.rows[i]["status"] & 1) == 1)) {
									votingID = votings.rows[i]["pkey"];
									break;
								}
							}
//console.log("votingID=",votingID);		
							if (votingID > 0) {
								//Заполняем поля голосования
								for (let i=0; i<this.fields.length; i++) {
									let value;
									if (this.fields[i]["fieldtype"] == 0) { //0 - строка
										value = this.createForm.get("field_"+this.fields[i]["name"]).value;
									} else
									if (this.fields[i]["fieldtype"] == 1) { //1 - имя аккаунта
										value = this.createForm.get("field_"+this.fields[i]["name"]).value;
									} else
									if (this.fields[i]["fieldtype"] == 2) { //2 - публичный ключ
										value = this.createForm.get("field_"+this.fields[i]["name"]).value;
									} else
									if (this.fields[i]["fieldtype"] == 3) { //3 - p2p-адрес
										value = this.createForm.get("field_"+this.fields[i]["name"]+"_ip").value + ":" +
												this.createForm.get("field_"+this.fields[i]["name"]+"_port").value;
									} else
									if (this.fields[i]["fieldtype"] == 4) { //4 - георасположение
										value = "lat:" + this.createForm.get("field_"+this.fields[i]["name"]+"_lat").value + ";" +
												"lon:" + this.createForm.get("field_"+this.fields[i]["name"]+"_lon").value + ";" +
												"place:" + this.createForm.get("field_"+this.fields[i]["name"]+"_place").value + ";";
									} else
									if (this.fields[i]["fieldtype"] == 111) {  //111 - файл
									}

									const ftr = this.dars.dars.transaction({
										actions: [{
											account: 'dars.compose',
											name: 'addvalue',
											authorization: [{
												actor: this.aService.selected.getValue().name,
												permission: 'active'
											}],
											data: {
												owner:   		this.aService.selected.getValue().name,
												votingid:   	votingID,
												fieldid:		this.fields[i]["id"],
												value:			value
											}
										}]
									}).then((result) => {
//console.log("addvalue.result=",result);		
										if (result.broadcast === true) {
											if (i == (this.fields.length-1)) {
												this.busy = false;
												this.wrongpass = '';
												this.createModal = false;
												this.showToast('успешно', 'Ваше голосование создано', '');
												this.aService.refreshFromChain();
												this.reloadVotings();
											}
										} else {
											this.busy = false;
											this.wrongpass = 'Ошибка отправки поля ['+this.fields[i]["comment"]+"]";
										}
									}).catch((error) => {
//console.log("addvalue.error=",error);		
										this.busy = false;
										this.wrongpass = 'Ошибка отправки поля ['+this.fields[i]["comment"]+']';
									});
								}	
							} else {
								this.busy = false;
								this.wrongpass = 'Ошибка создания голосования!';
							}
						}); //then((votings)
					}, 2000); //setTimeout
				} else {
					this.busy = false;
					this.wrongpass = JSON.parse(result).error.details[0].message;
				}
			}).catch((error) => {
//console.log("addvoting.error=", error);		
				this.busy = false;
				this.wrongpass = 'Ошибка отправки!';
			});
        } else {
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
        this.wrongpass = 'Неверный пароль!';
      });
  }

  editVoting(votingid) {
	this.votingID = votingid;
	this.selectedVotingType = 0;
	this.editForm.reset();
	this.wrongpass = "";
	this.wrongvoting = "";
	this.loadedFields = false;	  

	for (let i=0; i<this.votings.length; i++)
		if (this.votings[i]["id"] == votingid) {
			this.votingDetail = this.votings[i]; 
			this.selectedVotingType = this.votings[i]["typeid"];
			this.editForm.controls["votingcomment"].patchValue(this.votings[i]["comment"]);
			break;
		}

	let existent_fields = [];
	for (const fieldname in this.editForm.controls)
		existent_fields.unshift(fieldname);
	for (let i=0; i < existent_fields.length; i++) {
		if (existent_fields[i].indexOf("field_") === 0) {
			this.editForm.removeControl(existent_fields[i]);
		}
	}
		
	this.values = [];  
	this.dars.getVotingValues(votingid).then((values) => {
		values.rows.forEach((line) => {
			const obj = {
				id:  	  	line["pkey"],
				votingid:  	line["votingid"],
				fieldid:  	line["fieldid"],
				value:  	line["value"]
			};
			this.values.unshift(obj);
		});
//console.log("values=", this.values);
	   
		this.dars.getVotingFields(this.votingDetail["typeid"]).then((fields) => {
			this.fields = [];
			fields.rows.forEach((line) => {
				let value = line["defaultvalue"];
				for (let i=0; i<this.values.length; i++)
					if (this.values[i]["fieldid"] == line["pkey"]) {
						value = this.values[i]["value"];
						break;
					}
				const obj = {
					id:  	  	line["pkey"],
					visible:  	line["visible"],
					num:   		line["num"],
					required:   line["required"],
					name:    	line["name"],
					comment:  	line["comment"],
					fieldtype:  line["fieldtype"],
					value:  	value
				};
				
				if (obj["fieldtype"] == 0) { //0 - строка
					if (obj["required"]) {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required])));
					} else {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([])));
					}
				} else
				if (obj["fieldtype"] == 1) { //1 - имя аккаунта
					if (obj["required"]) {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required, Validators.minLength(12)])));
					} else {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.minLength(12)])));
					}
				} else
				if (obj["fieldtype"] == 2) { //2 - публичный ключ
					if (obj["required"]) {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([Validators.required])));
					} else {
						this.editForm.addControl("field_"+obj["name"], this.fb.control(obj["value"], Validators.compose([])));
					}
				} else
				if (obj["fieldtype"] == 3) { //3 - p2p-адрес
					let arr1 = obj["value"].split(":");
					obj["value_ip"] = arr1[0];
					obj["value_port"] = arr1[1];
					if (obj["required"]) {
						this.editForm.addControl("field_"+obj["name"]+"_ip",   this.fb.control(obj["value_ip"], Validators.compose([Validators.required])));
						this.editForm.addControl("field_"+obj["name"]+"_port", this.fb.control(obj["value_port"], Validators.compose([Validators.required])));
					} else {
						this.editForm.addControl("field_"+obj["name"]+"_ip",   this.fb.control(obj["value_ip"], Validators.compose([])));
						this.editForm.addControl("field_"+obj["name"]+"_port", this.fb.control(obj["value_port"], Validators.compose([])));
					}
				} else
				if (obj["fieldtype"] == 4) { //4 - георасположение
					let arr1 = obj["value"].split(";");
					for (let i=0; i<arr1.length; i++) {
						let arr2 = arr1[i].split(":");
						obj["value_"+arr2[0]] = arr2[1];
					}

					if (obj["required"]) {
						this.editForm.addControl("field_"+obj["name"]+"_lat",   this.fb.control(obj["value_lat"], Validators.compose([Validators.required])));
						this.editForm.addControl("field_"+obj["name"]+"_lon",   this.fb.control(obj["value_lon"], Validators.compose([Validators.required])));
						this.editForm.addControl("field_"+obj["name"]+"_place", this.fb.control(obj["value_place"], Validators.compose([Validators.required])));
					} else {
						this.editForm.addControl("field_"+obj["name"]+"_lat",   this.fb.control(obj["value_lat"], Validators.compose([])));
						this.editForm.addControl("field_"+obj["name"]+"_lon",   this.fb.control(obj["value_lon"], Validators.compose([])));
						this.editForm.addControl("field_"+obj["name"]+"_place", this.fb.control(obj["value_place"], Validators.compose([])));
					}
				} else
				if (obj["fieldtype"] == 111) {  //111 - файл
				}
				
				this.fields.unshift(obj);
			});
//console.log("fields=", this.fields);
			
			this.fields.sort(function(a, b) {
				var valueA, valueB;
				valueA = a["num"];
				valueB = b["num"];
				if (valueA > valueB) {
					return 1;
				}
				else if (valueA < valueB) {
					return -1;
				}
				return 0;
			});
			this.loadedFields = true;
			this.editModal = true;
		}).catch((err) => {
//console.log("fields err=", err);
		});
	}).catch((err) => {
//console.log("values err=", err);
	});
  }
  
  sendEditedVoting() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
//console.log("publicKey=",publicKey);		
    this.crypto.authenticate(this.editForm.get('pass').value, publicKey).then((res) => {
//console.log("crypto.authenticate.res=",res);		
        if (res) {
			this.dars.addAccountPermission(this.aService.selected.getValue().name, 'dars.compose');
			
			this.busy = true;
			//Редактируем голосование
			const tr = this.dars.dars.transaction({
				actions: [{
					account: 'dars.compose',
					name: 'addvoting',
					authorization: [{
						actor: this.aService.selected.getValue().name,
						permission: 'active'
					}],
					data: {
						votingtypeid:   this.selectedVotingType,
						status:			1,
						begints:		this.moment().format('X'),
						owner:   		this.aService.selected.getValue().name,
						purpose:		"dars",									//Изменить для голосования за фирму !!!
						comment:		this.editForm.get('votingcomment').value,
						pkey:			this.votingID
					}
				}]
			}).then((result) => {
//console.log("addvoting.result=",result);		
				if (result.broadcast === true) {
					//Заполняем поля голосования
					for (let i=0; i<this.fields.length; i++) {
						let value;
						if (this.fields[i]["fieldtype"] == 0) { //0 - строка
							value = this.editForm.get("field_"+this.fields[i]["name"]).value;
						} else
						if (this.fields[i]["fieldtype"] == 1) { //1 - имя аккаунта
							value = this.editForm.get("field_"+this.fields[i]["name"]).value;
						} else
						if (this.fields[i]["fieldtype"] == 2) { //2 - публичный ключ
							value = this.editForm.get("field_"+this.fields[i]["name"]).value;
						} else
						if (this.fields[i]["fieldtype"] == 3) { //3 - p2p-адрес
							value = this.editForm.get("field_"+this.fields[i]["name"]+"_ip").value + ":" +
									this.editForm.get("field_"+this.fields[i]["name"]+"_port").value;
						} else
						if (this.fields[i]["fieldtype"] == 4) { //4 - георасположение
							value = "lat:" + this.editForm.get("field_"+this.fields[i]["name"]+"_lat").value + ";" +
									"lon:" + this.editForm.get("field_"+this.fields[i]["name"]+"_lon").value + ";" +
									"place:" + this.editForm.get("field_"+this.fields[i]["name"]+"_place").value + ";";
						} else
						if (this.fields[i]["fieldtype"] == 111) {  //111 - файл
						}
						
						const ftr = this.dars.dars.transaction({
							actions: [{
								account: 'dars.compose',
								name: 'addvalue',
								authorization: [{
									actor: this.aService.selected.getValue().name,
									permission: 'active'
								}],
								data: {
									owner:   		this.aService.selected.getValue().name,
									votingid:   	this.votingID,
									fieldid:		this.fields[i]["id"],
									value:			value
								}
							}]
						}).then((result) => {
//console.log("addvalue.result=",result);		
							if (result.broadcast === true) {
								if (i == (this.fields.length-1)) {
									this.busy = false;
									this.wrongpass = '';
									this.editModal = false;
									this.showToast('успешно', 'Ваше голосование изменено', '');
									this.aService.refreshFromChain();
									this.reloadVotings();
								}
							} else {
								this.busy = false;
								this.wrongpass = 'Ошибка отправки поля ['+this.fields[i]["comment"]+"]";
							}
						}).catch((error) => {
//console.log("addvalue.error=",error);		
							this.busy = false;
							this.wrongpass = 'Ошибка отправки поля ['+this.fields[i]["comment"]+']';
						});
					} //for fields	
				} else {
					this.busy = false;
					this.wrongpass = 'Ошибка редактирования голосования!';
				}
			}).catch((error) => {; //then((votings)
//console.log("addvote.error=",error);		
				this.busy = false;
				this.wrongpass = 'Ошибка редактирования голосования!';
			});
		} else {
			this.wrongpass = 'Неверный пароль!';
		}
    }).catch((err) => {
        this.wrongpass = 'Неверный пароль!';
    });
}

  deleteVoting(votingid) {
	this.votingID = votingid;
	for (let i=0; i<this.votings.length; i++)
		if (this.votings[i]["id"] == votingid) {
			this.votingDetail = this.votings[i]; 
			break;
		}
		
	this.deleteForm.reset();
	this.wrongpass = "";
	this.deleteModal = true;
  }

  sendDeletedVoting() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.deleteForm.get('pass').value, publicKey).then((res) => {
        if (res) {
			this.busy = true;
			const tr = this.dars.dars.transaction({
				actions: [{
					account: 'dars.compose',
					name: 'delvoting',
					authorization: [{
						actor: this.aService.selected.getValue().name,
						permission: 'active'
					}],
					data: {
						owner:   	this.aService.selected.getValue().name,
						pkey:   	this.votingID
					}
				}]
			}).then((result) => {
				if (result.broadcast === true) {
					this.busy = false;
					this.wrongpass = '';
					this.deleteModal = false;
					this.showToast('успешно', 'Голосование удалено', '');
					this.aService.refreshFromChain();
					this.reloadVotings();
				} else {
					this.busy = false;
					this.wrongpass = JSON.parse(result).error.details[0].message;
				}
			}).catch((error) => {
				this.busy = false;
				this.wrongpass = 'Ошибка отправки!';
			});
        } else {
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
        console.log(err);
        this.wrongpass = 'Неверный пароль!';
      });
  }
  
  doVoting(votingid, vote) {
	this.votingID = votingid;
	this.voteResult = vote;
	if (vote == 1)
		this.voteResultStr = "ЗА";
	if (vote == -1)
		this.voteResultStr = "ПРОТИВ";
	  
	this.voteForm.reset();
	this.wrongpass = "";
	this.voteModal = true;
  }
  
  sendVote() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.voteForm.get('pass').value, publicKey).then((res) => {
        if (res) {
			this.busy = true;
			const tr = this.dars.dars.transaction({
				actions: [{
					account: 'dars.compose',
					name: 'addvote',
					authorization: [{
						actor: this.aService.selected.getValue().name,
						permission: 'active'
					}],
					data: {
						votingid:   this.votingID,
						owner:   	this.aService.selected.getValue().name,
						result: 	this.voteResult
					}
				}]
			}).then((result) => {
				if (result.broadcast === true) {
					this.busy = false;
					this.wrongpass = '';
					this.voteModal = false;
					this.showToast('успешно', 'Ваш голос отправлен', '');
					this.aService.refreshFromChain();
					this.reloadVotings();
				} else {
					this.busy = false;
					this.wrongpass = JSON.parse(result).error.details[0].message;
				}
			}).catch((error) => {
				this.busy = false;
				this.wrongpass = 'Ошибка отправки!';
			});
        } else {
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
        console.log(err);
        this.wrongpass = 'Неверный пароль!';
      });
  }
  
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

}
