import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataTable} from 'primeng/datatable';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {AccountsService} from '../../accounts.service';
import {CryptoService} from '../../services/crypto.service';
import {DARSJSService} from '../../darsjs.service';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';

import * as moment from 'moment';

//.............................................................................................	  
import * as DARSJS from '../../../assets/eos.js';
//.............................................................................................	  

@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.css']
})

export class RentComponent implements OnInit, AfterViewInit, OnDestroy {
  moment: any;
  rentouts = [];
  rentoutsa = [];
  totalRentOuts: number;
  totalRentOutsA: number;
  DRSBalance: number;
  loadingRentOuts: boolean;
  loadingRentOutsA: boolean;
  
  @ViewChild('rentOutsTable') public rentOutsTable: DataTable;
  @ViewChild('rentOutsATable') public rentOutsATable: DataTable;

  config: ToasterConfig;
  
  confirmForm:  FormGroup;
  confirmRefundForm:  FormGroup;
  paymentId:    number;
  fromAccount:  string;
  toAccount:    string;
  sendQuantity: string;
  minRefundDate: string;
  paymentMemo:  string;
  paymentComment:  string;
  wrongpass:    string;
  amounterror:  string;
  numberMask = createNumberMask({
    prefix: '',
    allowDecimal: true,
    includeThousandsSeparator: false,
    decimalLimit: 8,
  });

  busy: boolean;
  sendModal: boolean;
  refundModal: boolean;

  constructor(private fb: FormBuilder,
              public aService: AccountsService, 
			  public dars: DARSJSService,
			  private crypto: CryptoService,
			  private toaster: ToasterService) {
    this.moment = moment;
    this.rentouts = [];
    this.rentoutsa = [];
	this.totalRentOuts = 0;
	this.totalRentOutsA = 0;
    this.loadingRentOuts = true;
    this.loadingRentOutsA = true;
	
	this.confirmForm = this.fb.group({
	  refunddate: [''],
	  amount: ['', Validators.required],
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.confirmRefundForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(10)]]
    });
	this.busy = false;
	this.sendModal = false;
	this.refundModal = false;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.aService.selected.asObservable().subscribe((sel) => {
      if (sel['name']) {
        setTimeout(() => {
		  this.DRSBalance = sel.drs_balance;
          this.reloadRentOuts(sel.name);
          this.reloadRentOutsA(sel.name);
          this.aService.refreshFromChain();
        }, 50);
      }
    });
  }

  refreshFrom() {
    this.reloadRentOuts(this.aService.selected.getValue().name);
    this.reloadRentOutsA(this.aService.selected.getValue().name);
  }
  
  reloadRentOuts(account) {
    this.dars.getRentOuts(account).then((rentouts) => {
     if (rentouts.rows.length > 0) {
		this.rentouts = [];
		rentouts.rows.forEach((line) => {
			if (line["from"] == account) {
				let createdate = this.moment.unix(line["createts"]).format("YYYY-MM-DD HH:mm:ss");
				let refunddate = this.moment.unix(line["refundts"]).format("YYYY-MM-DD HH:mm:ss");
				const obj = {
					id:  	  	line["pkey"],
					createdate: createdate,
					from:  	  	line["from"],
					amount:   	line["amount"],
					dividend:   line["dividend"],
					refunddate: refunddate,
					locked:     line["locked"],
					voted:      line["voted"],
					producer:   line["producer"]
				};
				this.rentouts.unshift(obj);
			}
		});
		this.totalRentOuts = this.rentouts.length;
      } else {
		this.rentouts = [];
		this.totalRentOuts = 0;
	  }
	 this.loadingRentOuts = false;
	 this.rentOutsTable.reset();
    }).catch((err) => {
	 this.loadingRentOuts = false;
	});
  }

  reloadRentOutsA(account) {
    this.dars.getRentOutsA(account).then((rentoutsa) => {
     if (rentoutsa.rows.length > 0) {
		this.rentoutsa = [];
		rentoutsa.rows.forEach((line) => {
			if (line["from"] == account) {
				let createdate = this.moment.unix(line["createts"]).format("YYYY-MM-DD HH:mm:ss");
				let refunddate = this.moment.unix(line["refundts"]).format("YYYY-MM-DD HH:mm:ss");
				const obj = {
					id:  	  	line["pkey"],
					createdate: createdate,
					from:  	  	line["from"],
					amount:   	line["amount"],
					dividend:   line["dividend"],
					refunddate: refunddate,
					locked:     line["locked"]
				};
				this.rentoutsa.unshift(obj);
			}
		});
		this.totalRentOutsA = this.rentoutsa.length;
      } else {
		this.rentoutsa = [];
		this.totalRentOutsA = 0;
	  }
	 this.loadingRentOutsA = false;
	 this.rentOutsATable.reset();
    }).catch((err) => {
	 this.loadingRentOutsA = false;
	});
  }

  doRentOut() {
	this.confirmForm.reset();
	this.fromAccount = this.aService.selected.getValue().name;
	this.toAccount = "dars.rent";
	this.sendQuantity = "0.00000000";
	this.paymentMemo = "";
	this.paymentComment = "Сдача токенов в аренду";
	this.amounterror = "";
	this.wrongpass = "";
	this.confirmForm.controls['refunddate'].setValue(this.moment().add(1, 'M').format('YYYY-MM-DD'));
	this.minRefundDate = this.moment().add(1, 'd').format('YYYY-MM-DD');
	this.sendModal = true;
  }

  doRefundFrom(id) {
	this.confirmRefundForm.reset();
	this.paymentId = id;
	this.fromAccount = this.aService.selected.getValue().name;
	this.wrongpass = "";
	this.refundModal = true;
  }
    
  checkAmount() {
    if (parseFloat(this.confirmForm.get('amount').value) === 0 || 
	   this.confirmForm.get('amount').value === '') {
      this.confirmForm.controls['amount'].setErrors({'incorrect': true});
      this.amounterror = 'неверное количество';
    } else {
      this.confirmForm.controls['amount'].setErrors(null);
      this.amounterror = '';
    }
  }

  transfer() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.confirmForm.get('pass').value, publicKey).then((res) => {
        if (res) {
		  this.dars.addAccountPermission(this.fromAccount, 'dars.rent');
			
		  const amount = parseFloat(this.confirmForm.get('amount').value);
		  let precision = 8;
		  const refundts = this.moment(this.confirmForm.get('refunddate').value).format("X");
		  
			if (amount > this.DRSBalance) {
				this.wrongpass = 'Указанная сумма больше, чем количество токенов на Вашем балансе!';
			} else {
				if (refundts > this.moment().format("X")) {
					this.busy = true;
					const tr = this.dars.dars.transaction({
						actions: [{
							account: 'dars.rent',
							name: 'add',
							authorization: [{
								actor: this.aService.selected.getValue().name,
								permission: 'active'
							}],
							data: {
								from: this.aService.selected.getValue().name,
								amount: amount.toFixed(precision) + ' DRS',
								refundts: refundts
							}
						}],
					}).then((result) => {
						if (result.broadcast === true) {
							this.busy = false;
							this.wrongpass = '';
							this.sendModal = false;
							this.showToast('успешно', 'Транзакция отправлена', 'Информация о транзакциях отображается в разделе История.');
							this.aService.refreshFromChain();
							this.reloadRentOuts(this.fromAccount);
							this.reloadRentOutsA(this.fromAccount);
						} else {
							this.busy = false;
							this.wrongpass = JSON.parse(result).error.details[0].message;
						}
					}).catch((error) => {
						this.busy = false;
						this.wrongpass = 'Ошибка отправки!';
						console.log('Catch2', error);
					});
				} else {
					this.wrongpass = 'Укажите верную дату окончания аренды!';
				}
			} //amount > this.DRSBalance
        } else {
         this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
       this.wrongpass = 'Неверный пароль!';
       console.log(err);
      });
  }

  transferRefund() {
    const publicKey = this.aService.getPublicKey(this.aService.selected.getValue().details['permissions'], 'active');
    this.crypto.authenticate(this.confirmRefundForm.get('pass').value, publicKey).then((res) => {
        if (res) {
		  this.dars.addAccountPermission(this.fromAccount, 'dars.rent');
			
		  this.busy = true;
		  
		  const tr = this.dars.dars.transaction({
			actions: [{
				account: 'dars.rent',
				name: 'del',
				authorization: [{
					actor: this.aService.selected.getValue().name,
					permission: 'active'
				}],
				data: {
					pkey: this.paymentId
				}
			}],
		  }).then((result) => {
		  if (result.broadcast === true) {
			  this.busy = false;
              this.wrongpass = '';
              this.refundModal = false;
              this.showToast('успешно', 'Транзакция отправлена', 'Информация о транзакциях отображается в разделе История.');
              this.aService.refreshFromChain();
			  this.reloadRentOuts(this.fromAccount);
			  this.reloadRentOutsA(this.fromAccount);
            } else {
			  this.busy = false;
              this.wrongpass = JSON.parse(result).error.details[0].message;
			}
          }).catch((error) => {
			this.busy = false;
            console.log('Catch2', error);
            this.wrongpass = 'Ошибка отправки!';
          });
        } else {
          this.wrongpass = 'Неверный пароль!';
        }
      }).catch((err) => {
        console.log(err);
        this.wrongpass = 'Неверный пароль!';
      });
  }
  
  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

}
