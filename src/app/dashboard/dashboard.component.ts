import {Component, OnInit, NgZone, ViewChild} from '@angular/core';
import {DARSJSService} from '../darsjs.service';
import {AccountsService} from '../accounts.service';
import {LandingComponent} from '../landing/landing.component';
import {ClrWizard} from '@clr/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';

import * as moment from 'moment';
import {CryptoService} from '../services/crypto.service';
import {createNumberMask} from 'text-mask-addons/dist/textMaskAddons';
import {DARSAccount} from '../interfaces/account';

//.............................................................................................	  
import * as DARSJS from '../../assets/eos.js';
//.............................................................................................	  

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('wizardnew') wizardnew: ClrWizard;
  @ViewChild('importAccountWizard') importwizard: ClrWizard;
  lottieConfig: Object;
  anim: any;
  busy = false;
  
  refreshTracker: any;
  hasNewInvoices: boolean = false;
  
  accounts: any;
  importedAccount: any;
  appVersion: string;

  passform: FormGroup;
  newWallet: boolean;
  importKeyModal: boolean;
  deleteAccModal: boolean;
  swapForm: FormGroup;
  swapTokensModal: boolean;
  wrongpass: string;

  newAccountData = {
    t: 0,
    n: '',
    o: '',
    a: ''
  };
  payloadError = false;
  payloadValid = false;
  memo: string;
  newAccountPayload = '';
  newAccOptions = 'thispk';
  accountname = '';
  accountname_valid = false;
  accountname_err = '';
  amounterror = '';
  amounterror2 = '';
  amounterror3 = '';
  unstaked:number;
  passmatch = false;

  pvtform: FormGroup;
  passform2: FormGroup;
  passmatch2 = false;
  errormsg: string;
  importedAccounts: any[];

  ownerpk = '';
  ownerpub = '';
  activepk = '';
  activepub = '';
  importactivepk = '';
  hastokens = false;
  agreeKeys = false;
  generating = false;
  generated = false;

  final_creator = '';
  final_active = '';
  final_owner = '';
  final_name = '';

  confirmationID = '';
  success = false;

  wrongwalletpass = '';
  config: ToasterConfig;

  numberMask = createNumberMask({
    prefix: '',
    allowDecimal: true,
    includeThousandsSeparator: false,
    decimalLimit: 8,
  });

  intMask = createNumberMask({
    prefix: '',
    allowDecimal: false,
    includeThousandsSeparator: false
  });

  selectedAccRem = null;
  accRemovalIndex = null;
  selectedAccSwap = null;
  selectedTab = '';
  importedPublicKey = '';

  constructor(
    public  dars: DARSJSService,
    private fb: FormBuilder,
    public  aService: AccountsService,
    private toaster: ToasterService,
    private crypto: CryptoService,
    private zone: NgZone
  ) {
    this.newWallet = false;
    this.importKeyModal = false;
    this.deleteAccModal = false;
    this.swapTokensModal = false;
    this.appVersion = window['appversion'];

    this.passform = this.fb.group({
      matchingPassword: this.fb.group({
        pass1: ['', [Validators.required, Validators.minLength(10)]],
        pass2: ['', [Validators.required, Validators.minLength(10)]]
      })
    });
	
    this.pvtform = this.fb.group({
      private_key: ['', Validators.required]
    });
	
    this.passform2 = this.fb.group({
      matchingPassword: this.fb.group({
        pass1: ['', [Validators.required, Validators.minLength(10)]],
        pass2: ['', [Validators.required, Validators.minLength(10)]]
      })
    });

    this.swapForm = this.fb.group({
     icoemail: ['', [Validators.required]],
     icopass:  ['', [Validators.required]],
     pass: ['', [Validators.required, Validators.minLength(10)]]
    });

	this.wrongpass = '';
    this.errormsg = '';
    this.importedAccounts = [];
	this.refreshTracker = null;
  }

  verifyPrivateKey(input) {
    if (input !== '') {
      this.dars.checkPvtKey(input).then((results) => {
        this.importedPublicKey = results.publicKey;
        this.importedAccounts = [];
        this.importedAccounts = [...results.foundAccounts];
        this.pvtform.controls['private_key'].setErrors(null);
        this.zone.run(() => {
          this.importwizard.forceNext();
          this.errormsg = '';
        });
      }).catch((e) => {
        this.zone.run(() => {
          this.pvtform.controls['private_key'].setErrors({'incorrect': true});
          this.importedAccounts = [];
          if (e.message.includes('Invalid checksum')) {
            this.errormsg = 'Неверный приватный ключ';
          }
          if (e.message === 'no_account') {
            this.errormsg = 'Нет кошелька, который связан с введенным приватным ключем!';
          }
          if (e.message === 'non_active') {
            this.errormsg = 'Этот ключ неактивный! Пожалуйста, укажите именно активный ключ!';
          }
        });
      });
    }
  }

  doCancel(): void {
    this.importwizard.close();
  }

  importAccounts() {
    if (this.passform2.value.matchingPassword.pass1 === this.passform2.value.matchingPassword.pass2) {
      this.crypto.initKeys(this.importedPublicKey, this.passform2.value.matchingPassword.pass1).then(() => {
        this.crypto.encryptAndStore(this.pvtform.value.private_key, this.importedPublicKey).then(() => {
          this.passform2.reset();
          this.importwizard.reset();
          this.pvtform.reset();
          this.aService.appendAccounts(this.importedAccounts);
        }).catch((err) => {
          console.log(err);
        });
      });
    }
  }

  openRemoveAccModal(index, account) {
    this.selectedAccRem = account;
    this.accRemovalIndex = index;
    this.deleteAccModal = true;
  }

  doRemoveAcc() {
    this.aService.accounts.splice(this.accRemovalIndex, 1);
    this.deleteAccModal = false;
    this.aService.select(0);
    this.selectedTab = '0';
  }

  openSwapTokensModal(account) {
	this.swapForm.reset();
    this.selectedAccSwap = account;
    this.swapTokensModal = true;
  }

  doSwapTokens() {
    const publicKey = this.aService.getPublicKey(this.selectedAccSwap.details['permissions'], 'active');
    this.crypto.authenticate(this.swapForm.get('pass').value, publicKey).then((res) => {
        if (res) {
			const email = this.swapForm.get('icoemail').value;
			const pass = this.swapForm.get('icopass').value;
			
			const passhash = this.crypto.sha256(pass).toString();
			const passemailhash = this.crypto.sha256(passhash+"-" + email.replace('.','')).toString();
//console.log("email=[",email,"] pass=[",pass,"] passhash=[",passhash,"] passemailhash=[",passemailhash,"]");			
			
			this.busy = true;
			const tr = this.dars.dars.transaction({
				actions: [{
					account: 'dars.swap',
					name: 	 'gettokens',
					authorization: [{
						actor: 		this.selectedAccSwap.name,
						permission: 'active'
					}],
					data: {
						user:     	this.selectedAccSwap.name,
						email_pass: passemailhash
					}
				}]
			}).then((result) => {
				if (result.broadcast === true) {
					this.busy = false;
					this.wrongpass = '';
					this.swapTokensModal = false;
					this.showToast('успешно', 'Токены обменяны', 'Информация о полученных токенах отображается в разделе Аренда.');
					this.aService.refreshFromChain();
				} else {
					this.busy = false;
					this.wrongpass = JSON.parse(result).error.details[0].message;
				}
			}).catch((error) => {
				this.busy = false;
				this.wrongpass = 'Ошибка отправки! Проверьте все параметры!';
				console.log('Catch2', error);
			});
        } else {
			this.wrongpass = 'Неверный пароль!';
        }
    }).catch((err) => {
		this.wrongpass = 'Неверный пароль!';
		console.log(err);
    });
  }

  resetAndClose() {
    this.wizardnew.reset();
    this.wizardnew.close();
  }

  doWalletFinish() {
//.............................................................................................	  
	const config = {
		keyProvider: ['5KUd7p1BBgCWz6ZYJYB6roMHKNZExWxzJ2qGactdDEse2Tdr9sz'], //dars.testup
		httpEndpoint: 'http://159.69.176.220:8888',
//		httpEndpoint: 'https://testproducer.dars.one',
		expireInSeconds: 60,
		broadcast: true,
		debug: false,
		sign: true,
		chainId: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f',
	};
    const tmpdars = DARSJS(config);

	const tr = tmpdars.transaction({
		actions: [{
			account: 'dars.signup',
			name: 'transfer',
			authorization: [{
				actor: 'dars.testup',
				permission: 'active'
			}],
			data: {
				from: 'dars.testup',
				to: 'dars.signup',
				quantity: '1000.00000000 DRS',
				memo: this.memo
			}
		}],
	});
//.............................................................................................	  
    this.importactivepk = this.activepk;

	this.wizardnew.forceFinish();
    this.wizardnew.reset();
	this.accountname = "";
	this.accountname_valid = false;
	this.ownerpub = "";
	this.activepub = "";
	this.ownerpk = "";
	this.activepk = "";
	this.hastokens = false;
	this.agreeKeys = false;
	this.memo = "";
    this.wizardnew.close();
	
	this.importKeyModal = true;
  }

  loadLastPage() {
    if (this.newAccOptions === 'newpk') {
      this.final_active = this.activepub;
      this.final_owner = this.ownerpub;
    }
    if (this.newAccOptions === 'thispk') {
      const account = this.aService.selected.getValue();
	  this.final_active = this.aService.getPublicKey(account.details['permissions'], 'active');
	  this.final_owner =  this.aService.getPublicKey(account.details['permissions'], 'owner');
    }
  }

  ngOnInit() {
    this.accounts = [];
    this.dars.status.asObservable().subscribe((status) => {
      if (status) {
        this.loadStoredAccounts();
      }
    });
	if (!this.refreshTracker) {
      this.refreshTracker = setInterval(() => {
        this.refreshBalanceAndInvoices();
      }, 10000);
	}
  }
    
  decodeAccountPayload(payload: string) {
    if (payload !== '') {
      if (payload.endsWith('=')) {
        try {
          const accountObj = JSON.parse(atob(payload));
          accountObj.t = moment(accountObj.t);
          console.log(accountObj);
          this.newAccountData = accountObj;
          this.final_name = accountObj.n;
          this.final_active = accountObj.a;
          this.final_owner = accountObj.o;
          this.final_creator = this.aService.selected.getValue().name;
          this.payloadError = false;
        } catch (e) {
          this.payloadError = true;
        }
      } else {
        this.payloadError = true;
      }
    }
  }

  handleAnimation(anim: any) {
    this.anim = anim;
    this.anim['setSpeed'](0.8);
  }

  refreshBalanceAndInvoices() {
	console.log("refreshing balance and invoices...");
	this.aService.refreshFromChain();
	
	this.hasNewInvoices = false;
    this.dars.getInvoices(this.aService.selected.getValue().name).then((invoices) => {
        if (invoices.rows.length > 0) {
			invoices.rows.forEach((invoice) => {
				if ((invoice["from"] == this.aService.selected.getValue().name) && 
				    (invoice["payed"] == 0) &&
					(invoice["timestamp"] >= (Math.floor(Date.now()/1000) - 24*3600))) {
					this.hasNewInvoices = true;
				}
			});
        }
    });  
  }
  
  selectAccount(idx) {
    this.selectedTab = idx;
    this.aService.select(idx);
	this.refreshBalanceAndInvoices();
  }

  loadStoredAccounts() {
    const account_names = Object.keys(this.dars.accounts.getValue());
    if (account_names.length > 0) {
      account_names.forEach((name) => {
        const acc = this.dars.accounts.getValue()[name];
        let drs_balance = 0;
        let dusd_balance = 0;
        acc['tokens'].forEach((tk) => {
		  if (tk.split(' ')[1] === 'DRS') {	
           drs_balance += LandingComponent.parseDRS(tk);
		  } else
		  if (tk.split(' ')[1] === 'DUSD') {	
           dusd_balance += LandingComponent.parseDUSD(tk);
		  }
        });
        const net = LandingComponent.parseDRS(acc['total_resources']['net_weight']);
        const cpu = LandingComponent.parseDRS(acc['total_resources']['cpu_weight']);
        const accData = {
          name: acc['account_name'],
          drs_balance: Math.round((drs_balance) * 100000000) / 100000000,
          dusd_balance: Math.round((dusd_balance) * 100) / 100,
          staked: net + cpu,
          details: acc
        };
        this.accounts.push(accData);
        this.aService.accounts.push(accData);
      });
    }
    this.aService.initFirst();
  }

  cc(text, title, body) {
    this.showToast('успешно', title, body);
    if (typeof window['clipboard'] !== "undefined") {
     window['clipboard']['writeText'](text);
    } else {
     navigator['clipboard']['writeText'](text);
    }
  }

  verifyAccountName(next) {
    try {
      this.accountname_valid = false;
      const res = this.dars.checkAccountName(this.accountname);
      console.log(res);
      if (res !== 0) {
        if (this.accountname.length === 12) {
          this.dars.dars['getAccount'](this.accountname, (err, data) => {
            console.log(err, data);
            if (err) {
              this.accountname_valid = true;
              this.newAccountData.n = this.accountname;
              this.final_name = this.accountname;
              this.final_creator = this.aService.selected.getValue().name;
              this.accountname_err = '';
              if (next) {
                this.wizardnew.next();
              }
            } else {
              if (data) {
                this.accountname_err = 'Это имя кошелька занято. Попробуйте другое!';
                this.accountname_valid = false;
              }
            }
          });
        } else {
          this.accountname_err = 'Имя кошелька должно содержать строго 12 символов. a-z, 1-5';
          this.accountname_valid = false;
        }
      }
    } catch (e) {
      this.accountname_err = e.message;
      this.accountname_valid = false;
    }
  }
  
 changeAccountName() {
	  this.accountname_valid = false;
	  for (let i=0; i<this.accountname.length; i++) {
		  let c = this.accountname[i];
		  if (c == '.')
			  return;
	  }
	  if (this.accountname.length === 12) {
		  this.verifyAccountName(false)
	  }
  }
  
  passCompare() {
    const pForm = this.passform.value.matchingPassword;
    if (pForm.pass1 && pForm.pass2) {
      if (pForm.pass1 === pForm.pass2) {
        this.passform['controls'].matchingPassword['controls']['pass2'].setErrors(null);
        this.passmatch = true;
      } else {
        this.passform['controls'].matchingPassword['controls']['pass2'].setErrors({'incorrect': true});
        this.passmatch = false;
      }
    }
  }

  importedPassCompare() {
    const pForm = this.passform2.value.matchingPassword;
    if (pForm.pass1 && pForm.pass2) {
      if (pForm.pass1 === pForm.pass2) {
        this.passform2['controls'].matchingPassword['controls']['pass2'].setErrors(null);
        this.passmatch2 = true;
      } else {
        this.passform2['controls'].matchingPassword['controls']['pass2'].setErrors({'incorrect': true});
        this.passmatch2 = false;
      }
    }
  }

  generateKeys() {
    this.generating = true;
    setTimeout(() => {
      this.dars.ecc.initialize().then(() => {
        this.dars.ecc['randomKey'](128).then((privateKey) => {
          this.ownerpk = privateKey;
          this.ownerpub = this.dars.ecc['privateToPublic'](this.ownerpk);
          console.log(this.ownerpk, this.ownerpub);
          this.dars.ecc['randomKey'](128).then((privateKey2) => {
            this.activepk = privateKey2;
			this.importactivepk = privateKey2;
            this.activepub = this.dars.ecc['privateToPublic'](this.activepk);
            this.generating = false;
            this.generated = true;
            console.log(this.activepk, this.activepub);
          });
        });
      });
    }, 100);
  }
  
  makePayload() {
    if (this.dars.ecc['isValidPublic'](this.ownerpub) && this.dars.ecc['isValidPublic'](this.activepub)) {
      console.log('Generating account payload');
      this.newAccountPayload = btoa(JSON.stringify({
        n: this.accountname,
        o: this.ownerpub,
        a: this.activepub,
        t: new Date().getTime()
      }));
      this.payloadValid = true;
    } else {
      alert('Invalid public key!');
      this.newAccountPayload = 'Неверный публичный ключ! Пожалуйста, вернитесь на шаг и исправьте его!';
      this.payloadValid = false;
      this.wizardnew.navService.previous();
    }
  }

  makeMemo() {
    this.memo = this.accountname + '-' + this.ownerpub + '-' + this.activepub;
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }
}
