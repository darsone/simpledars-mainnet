import {Component, NgZone, OnInit, ViewChild, ElementRef} from '@angular/core';
import {DARSJSService} from '../darsjs.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountsService} from '../accounts.service';
import {Router} from '@angular/router';
import {ClrWizard} from '@clr/angular';
import {NetworkService} from '../network.service';
import {CryptoService} from '../services/crypto.service';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {forEach} from '@angular/router/src/utils/collection';

//.............................................................................................	  
import * as DARSJS from '../../assets/eos.js';
//.............................................................................................	  

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  @ViewChild('wizardexists') exisitswizard: ClrWizard;
  @ViewChild('wizardnew') wizardnew: ClrWizard;
  @ViewChild('wizardkeys') wizardkeys: ClrWizard;
  @ViewChild('customImportBK') customImportBK: ElementRef;
  lottieConfig: Object;
  anim: any;
  busy: boolean;
  existingWallet: boolean;
  newWallet: boolean;
  newKeys: boolean;
  accountname = '';
  accountname_err = '';
  accountname_valid = false;
  ownerpk = '';
  ownerpk2 = '';
  ownerpub = '';
  ownerpub2 = '';
  activepk = '';
  activepub = '';
  importactivepk = '';
  hastokens = false;
  newAccountPayload = '';
  agreeKeys = false;
  agreeKeys2 = false;
  check: boolean;
  publicDARS: string;
  checkerr: string;
  errormsg: string;
  accounts: any[];
  dropReady: boolean;
  passmatch: boolean;
  agree: boolean;
  agree2: boolean;
  generating = false;
  generating2 = false;
  passform: FormGroup;
  pvtform: FormGroup;
  importForm: FormGroup;
  pk: string;
  publickey: string;
  pin: string;
  lockscreen: boolean;
  lockscreen2: boolean;
  importedAccounts: any[];
  endpoint = 'http://159.69.176.220:8888';
//  endpoint = 'https://testproducer.dars.one';
  payloadValid = false;
  generated = false;
  generated2 = false;
  config: ToasterConfig;
  verifyPanel = false;
  choosedFil: string;
  disableIm: boolean;
  infile: any;
  total_amount: number;
  memo: string;
  busy2 = false;

  static parseDRS(tk_string) {
    if (tk_string.split(' ')[1] === 'DRS') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }

  static parseDUSD(tk_string) {
    if (tk_string.split(' ')[1] === 'DUSD') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }

  constructor(public dars: DARSJSService,
              private crypto: CryptoService,
              private fb: FormBuilder,
              private aService: AccountsService,
              private toaster: ToasterService,
              public network: NetworkService,
              private router: Router,
              private zone: NgZone) {
    this.busy = true;
    this.existingWallet = false;
    this.dropReady = false;
    this.newWallet = false;
    this.check = false;
    this.passmatch = true;
    this.agree = false;
    this.agree2 = false;
    this.lockscreen = false;
    this.lockscreen2 = false;
    this.disableIm = false;
    this.accounts = [];
    this.importedAccounts = [];
    this.checkerr = '';
    this.errormsg = '';
    this.total_amount = 1;
    this.memo = '';

    this.network.connect();
	
    this.network.networkingReady.asObservable().subscribe((status) => {
      this.busy = !status;
    });
	
    this.publicDARS = '';

    this.passform = this.fb.group({
      matchingPassword: this.fb.group({
        pass1: ['', [Validators.required, Validators.minLength(10)]],
        pass2: ['', [Validators.required, Validators.minLength(10)]]
      })
    });
    this.pvtform = this.fb.group({
      private_key: ['', Validators.required]
    });
    this.importForm = this.fb.group({
      pass: ['', Validators.required],
      customImportBK: ['', Validators.required],
    });
  }

  cc(text, title, body) {
    this.showToast('успешно', title, body);
    if (typeof window['clipboard'] !== "undefined") {
     window['clipboard']['writeText'](text);
    } else {
     navigator['clipboard']['writeText'](text);
    }
  }

  resetAndClose() {
    this.wizardnew.reset();
    this.wizardnew.close();
  }
  
  doWalletFinish() {
//.............................................................................................	  
	const config = {
		keyProvider: ['5KUd7p1BBgCWz6ZYJYB6roMHKNZExWxzJ2qGactdDEse2Tdr9sz'], //dars.testup
		httpEndpoint: 'http://159.69.176.220:8888',
//		httpEndpoint: 'https://testproducer.dars.one',
		expireInSeconds: 60,
		broadcast: true,
		debug: false,
		sign: true,
		chainId: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f',
	};
    const tmpdars = DARSJS(config);

	const tr = tmpdars.transaction({
		actions: [{
			account: 'dars.signup',
			name: 'transfer',
			authorization: [{
				actor: 'dars.testup',
				permission: 'active'
			}],
			data: {
				from: 'dars.testup',
				to: 'dars.signup',
				quantity: '1000.00000000 DRS',
				memo: this.memo
			}
		}],
	});
//.............................................................................................	  
	this.importactivepk = this.activepk;
  
	this.wizardnew.forceFinish();
    this.wizardnew.reset();
	this.accountname = "";
	this.accountname_valid = false;
	this.ownerpub = "";
	this.activepub = "";
	this.ownerpk = "";
	this.activepk = "";
	this.agreeKeys = false;
	this.hastokens = false;
	this.memo = "";
    this.wizardnew.close();
	
	this.existingWallet = true;
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'slideDown',
      limit: 1,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toaster.popAsync(toast);
  }

  ngOnInit() {
  }

  setPin() {
    setTimeout(() => {
      this.crypto.createPIN(this.pin);
    }, 4000);
  }

  verifyAccountName(next) {
    try {
      this.accountname_valid = false;
      const res = this.dars.checkAccountName(this.accountname);
      console.log(res);
      if (res !== 0) {
        if (this.accountname.length === 12) {
          this.dars.dars['getAccount'](this.accountname, (err, data) => {
            console.log(err, data);
            if (err) {
              this.accountname_valid = true;
              this.accountname_err = '';
              if (next) {
                this.wizardnew.next();
              }
            } else {
              if (data) {
                this.accountname_err = 'Это имя кошелька занято. Попробуйте другое!';
                this.accountname_valid = false;
              }
            }
          });
        } else {
          this.accountname_err = 'Имя кошелька должно содержать строго 12 символов. a-z, 1-5';
	      this.accountname_valid = false;
        }
      }
    } catch (e) {
      this.accountname_err = e.message;
 	  this.accountname_valid = false;
    }
  }
  
 changeAccountName() {
	  this.accountname_valid = false;
	  for (let i=0; i<this.accountname.length; i++) {
		  let c = this.accountname[i];
		  if (c == '.')
			  return;
	  }
	  if (this.accountname.length === 12) {
		  this.verifyAccountName(false)
	  }
  }
  
 generateKeys() {
    this.generating = true;
    setTimeout(() => {
      this.dars.ecc.initialize().then(() => {
        this.dars.ecc['randomKey'](128).then((privateKey) => {
          this.ownerpk = privateKey;
          this.ownerpub = this.dars.ecc['privateToPublic'](this.ownerpk);
          console.log(this.ownerpk, this.ownerpub);
          this.dars.ecc['randomKey'](128).then((privateKey2) => {
            this.activepk = privateKey2;
			this.importactivepk = privateKey2;
            this.activepub = this.dars.ecc['privateToPublic'](this.activepk);
            this.generating = false;
            this.generated = true;
            console.log(this.activepk, this.activepub);
          });
        });
      });
    }, 100);
  }

 generateNKeys() {
    this.generating2 = true;
    setTimeout(() => {
      this.dars.ecc.initialize().then(() => {
        this.dars.ecc['randomKey'](128).then((privateKey) => {
          this.ownerpk2 = privateKey;
          this.ownerpub2 = this.dars.ecc['privateToPublic'](this.ownerpk2);
          this.generating2 = false;
          this.generated2 = true;
          console.log(this.ownerpk2, this.ownerpub2);
        });
      });
    }, 100);
  }

  makePayload() {
    if (this.dars.ecc['isValidPublic'](this.ownerpub) && this.dars.ecc['isValidPublic'](this.activepub)) {
      console.log('Generating account payload');
      this.newAccountPayload = btoa(JSON.stringify({
        n: this.accountname,
        o: this.ownerpub,
        a: this.activepub,
        t: new Date().getTime()
      }));
      this.payloadValid = true;
    } else {
      alert('Invalid public key!');
      this.newAccountPayload = 'Неверный публичный ключ! Пожалуйста, вернитесь на шаг и исправьте его!';
      this.payloadValid = false;
      this.wizardnew.navService.previous();
    }
  }

  makeMemo() {
    this.memo = this.accountname + '-' + this.ownerpub + '-' + this.activepub;
  }

  retryConn() {
    this.network.connect();
  }

  customConnect() {
    this.network.startup(this.endpoint);
  }

  handleAnimation(anim: any) {
    this.anim = anim;
    this.anim['setSpeed'](0.8);
  }

  passCompare() {
    if (this.passform.value.matchingPassword.pass1 && this.passform.value.matchingPassword.pass2) {
      if (this.passform.value.matchingPassword.pass1 === this.passform.value.matchingPassword.pass2) {
        this.passform['controls'].matchingPassword['controls']['pass2'].setErrors(null);
        this.passmatch = true;
      } else {
        this.passform['controls'].matchingPassword['controls']['pass2'].setErrors({'incorrect': true});
        this.passmatch = false;
      }
    }
  }

  importCredentials() {
    if (this.passform.value.matchingPassword.pass1 === this.passform.value.matchingPassword.pass2) {
      this.crypto.initKeys(this.publicDARS, this.passform.value.matchingPassword.pass1).then(() => {
        this.crypto.encryptAndStore(this.pvtform.value.private_key, this.publicDARS).then(() => {
          this.aService.importAccounts(this.importedAccounts);
          this.crypto.decryptKeys(this.publicDARS).then(() => {
            this.router.navigate(['dashboard', 'send']).catch((err) => {
              console.log(err);
            });
            if (this.lockscreen) {
              this.setPin();
            }
          }).catch((error) => {
            console.log('Error', error);
          });
        }).catch((err) => {
          console.log(err);
        });
      });
    }
  }

  verifyPrivateKey(input, path) {
    if (input !== '') {
      this.dars.checkPvtKey(input).then((results) => {
        this.publicDARS = results.publicKey;
        this.importedAccounts = [];
        this.importedAccounts = [...results.foundAccounts];
        this.pvtform.controls['private_key'].setErrors(null);
        this.zone.run(() => {
          this.exisitswizard.forceNext();
          this.errormsg = '';
        });
      }).catch((e) => {
        this.zone.run(() => {
          this.dropReady = true;
          this.pvtform.controls['private_key'].setErrors({'incorrect': true});
          this.importedAccounts = [];
          if (e.message.includes('Invalid checksum')) {
            this.errormsg = 'Неверный приватный ключ';
          }
          if (e.message === 'no_account') {
            this.errormsg = 'Нет кошелька, который связан с введенным приватным ключем!';
          }
          if (e.message === 'non_active') {
            this.errormsg = 'Этот ключ неактивный! Пожалуйста, укажите именно активный ключ!';
          }
        });
      });
    }
  }

  doCancel(): void {
    this.exisitswizard.close();
  }

  checkAccount() {
    if (this.dars.ready) {
      this.check = true;
      this.accounts = [];
      this.dars.loadPublicKey(this.publicDARS).then((account_data: any) => {
        account_data.foundAccounts.forEach((acc) => {
          let drs_balance = 0;
          let dusd_balance = 0;
          // Parse tokens and calculate balance
          acc['tokens'].forEach((tk) => {
			if (tk.split(' ')[1] === 'DRS') {
             drs_balance += LandingComponent.parseDRS(tk);
			} else
			if (tk.split(' ')[1] === 'DUSD') {
             dusd_balance += LandingComponent.parseDUSD(tk);
			}
          });
          const accData = {
            name: acc['account_name'],
            drs_balance: Math.round((drs_balance) * 100000000) / 100000000,
            dusd_balance: Math.round((dusd_balance) * 100) / 100
          };
          this.accounts.push(accData);
        });
        this.checkerr = '';
      }).catch((err) => {
        console.log("checkAccount: err=", err);
        this.checkerr = err.message;
      });
    }
  }

  inputIMClick() {
    this.customImportBK.nativeElement.click();
    // let el: HTMLElement = this.customExportBK.nativeElement as HTMLElement;
    // el.click();
  }

  importCheckBK(a) {

    this.infile = a.target.files[0];

    const name = this.infile.name;

    if (name != 'simpledars.bkp') {
      this.showToast('Ошибка', 'Неверный файл!', '');
      this.infile = '';
      return false;
    }
    this.choosedFil = name;
  }
}
