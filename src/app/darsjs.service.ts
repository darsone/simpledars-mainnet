import {Injectable} from '@angular/core';

import * as DARSJS from '../assets/eos.js';
import * as BIGJS from '../assets/big.js';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable()
export class DARSJSService {
  darsio: any;
  tokens: any;
  public ecc: any;
  format: any;
  ready: boolean;
  status = new Subject<Boolean>();
  txh: any[];
  baseConfig = {
    keyProvider: [],
    httpEndpoint: '',
    expireInSeconds: 60,
    broadcast: true,
    debug: false,
    sign: true,
    chainId: ''
  };
  basePublicKey = '';
  auth = false;
  constitution = 'Правила пользования находятся в разработке';
  txCheckQueue = [];
  txMonitorInterval = null;

  public accounts = new BehaviorSubject<any>({});
  public online = new BehaviorSubject<boolean>(false);
  public chainID: string;
  public dars: any;

  constructor() {
    this.darsio = null;
    this.ecc = DARSJS.modules['ecc'];
    this.format = DARSJS.modules['format'];
    this.ready = false;
    this.txh = [];
  }

  reloadInstance() {
    this.auth = true;
    this.dars = DARSJS(this.baseConfig);
  }

  clearSigner() {
    console.log(this.dars);
  }

  loadNewConfig(signer) {
    this.dars = DARSJS({
      httpEndpoint: this.baseConfig.httpEndpoint,
      signProvider: signer,
      chainId: this.chainID,
      sign: true,
      broadcast: true
    });
  }

  init(url, chain) {
    this.chainID = chain;
    return new Promise((resolve, reject) => {
      this.baseConfig.chainId = this.chainID;
      this.baseConfig.httpEndpoint = url;
      this.dars = DARSJS(this.baseConfig);
      this.dars['getInfo']({}).then(result => {
        this.ready = true;
        this.online.next(result['head_block_num'] - result['last_irreversible_block_num'] < 400);
        let savedAcc = [];
        const savedpayload = localStorage.getItem('simpledars.accounts.' + this.chainID);
        if (savedpayload) {
          savedAcc = JSON.parse(savedpayload).accounts;
        }
        this.dars['contract']('dars').then(contract => {
          this.darsio = contract;
          resolve(savedAcc);
        });
      }).catch((err) => {
        reject(err);
      });
    });
  }

  getKeyAccounts(pubkey) {
    return this.dars.getKeyAccounts(pubkey);
  }

  getAccountInfo(name) {
    return this.dars['getAccount'](name);
  }
  
  addAccountPermission(account, contract) {
	  this.dars['getAccount'](account).then((data) => {
		  console.log('account data=', data);
		  data.permissions.forEach((permission) => {
			  console.log('permission=', permission);
			  if ((permission["perm_name"] == 'active') &&
			      (permission["parent"] == 'owner')){
				let found = false;
				permission.required_auth.accounts.forEach((account) => {
					console.log('account permission=', account);
					if ((account.permission.actor == contract) &&
						(account.permission.permission == 'active')) {
					  found = true;
					}
				});
				if (!found) {
					console.log("Adding new permission");
					const new_account_permission = {
						permission: {
							actor: contract,
							permission: 'active'
						},
						weight: 1
					};
		  
					permission.required_auth.accounts.unshift(new_account_permission);

					console.log('new auth=', permission.required_auth);
					this.dars['updateauth']({
						json: true,
						account: account,
						permission: 'active',
						parent: 'owner',
						auth: permission.required_auth 
					});
				}
			  }
		  });
	  });
  }

  getChainInfo(): Promise<any> {
    if (this.dars) {
      return this.dars['getTableRows']({
        json: true,
        code: 'dars',
        scope: 'dars',
        table: 'global'
      });
    } else {
      return new Promise(resolve => {
        resolve();
      });
    }
  }

  getVotingTypes(): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'votingtypes',
	  limit: 1000000
    });
  }

  getVotingFields(votingtypeid): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'fields',
	  limit: 1000000,
	  lower_bound: votingtypeid,
	  upper_bound: votingtypeid+1,
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getVotings(): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'votings',
	  limit: 1000000
    });
  }

  getVotingValues(votingid): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'values',
	  limit: 1000000,
	  lower_bound: votingid,
	  upper_bound: votingid+1,
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getVotingVotes(votingid): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'votes',
	  limit: 1000000,
	  lower_bound: votingid,
	  upper_bound: votingid+1,
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getVotingFiles(votingid): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.compose',
      scope: 'dars.compose',
      table: 'files',
	  limit: 1000000,
	  lower_bound: votingid,
	  upper_bound: votingid+1,
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getComissions(): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.invoice',
      scope: 'dars.invoice',
      table: 'feecommands',
	  limit: 1000000
    });
  }

  getInvoices(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.invoice',
      scope: 'dars.invoice',
      table: 'invoices',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getBonuses(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.bonus',
      scope: 'dars.bonus',
      table: 'bonuses',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getHistoriesFrom(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.token',
      scope: 'dars.token',
      table: 'history',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getHistoriesTo(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.token',
      scope: 'dars.token',
      table: 'history',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 3,
	  key_type: 'i64'
    });
  }

  getRentOuts(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.rent',
      scope: 'dars.rent',
      table: 'rentouts',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getRentOutsA(account): Promise<any> {
	const table_key = new BIGJS(this.format.encodeName(account, false));  
    return this.dars['getTableRows']({
      json: true,
      code: 'dars.rent',
      scope: 'dars.rent',
      table: 'rentoutsa',
	  limit: 1000000,
	  lower_bound: table_key.toString(),
	  upper_bound: table_key.plus(1).toString(),
	  index_position: 2,
	  key_type: 'i64'
    });
  }

  getProducers(): Promise<any> {
    return this.dars['getTableRows']({
      json: true,
      code: 'dars',
      scope: 'dars',
      table: 'producers',
	  limit: 1000000,
	  index_position: 2,
	  key_type: 'float64'
    });
  }
    
  checkAccountName(name) {
    return this.format['encodeName'](name);
  }

  loadPublicKey(pubkey) {
    return new Promise((resolve, reject) => {
      if (this.ecc['isValidPublic'](pubkey)) {
        this.getKeyAccounts(pubkey).then((data) => {
          if (data['account_names'].length > 0) {
            const promiseQueue = [];
            data['account_names'].forEach((acc) => {
              console.log(acc);
              const tempPromise = new Promise((resolve1, reject1) => {
                this.getAccountInfo(acc).then((acc_data) => {
				  let keyfound = false;	
				  acc_data.permissions.forEach((permission) => {
                   console.log("loadPublicKey: checking private key", permission['required_auth']['keys'][0].key);
				   if (permission["perm_name"] == 'active') {
                    if (permission['required_auth']['keys'][0].key === pubkey) {
                     console.log("loadPublicKey: found active private key", permission['required_auth']['keys'][0].key);
					 keyfound = true;
                     this.getTokens(acc_data['account_name']).then((tokens) => {
                      acc_data['tokens'] = tokens;
                      this.accounts[acc] = acc_data;
                      resolve1(acc_data);
                     }).catch((err) => {
                      console.log(err);
                      reject1();
                     });
                    }
				   }
				  });
				 if (!keyfound) {
				  reject({message: 'non_active'});
				 }
                });
              });
              promiseQueue.push(tempPromise);
            });
            Promise.all(promiseQueue).then((results) => {
              resolve({
                foundAccounts: results,
                publicKey: pubkey
              });
            }).catch(() => {
              reject({message: 'non_active'});
            });
          } else {
            reject({message: 'no_account'});
          }
        });
      } else {
        reject({message: 'invalid'});
      }
    });
  }

  storeAccountData(accounts) {
    if (accounts) {
      if (accounts.length > 0) {
        this.accounts.next(accounts);
        const payload = JSON.parse(localStorage.getItem('simpledars.accounts.' + this.chainID));
        payload.updatedOn = new Date();
        payload.accounts = accounts;
        localStorage.setItem('simpledars.accounts.' + this.chainID, JSON.stringify(payload));
      }
    }
  }

  listProducers() {
    return this.dars['getProducers']({json: true, limit: 200});
  }

  getTokens(name) {
    return this.dars['getCurrencyBalance']('dars.token', name);
  }

  getTransaction(hash) {
    if (this.ready) {
      this.dars['getTransaction'](hash).then((result) => {
        this.txh.push(result);
      });
    }
  }

  getConstitution() {
  }

  async transfer(contract, from, to, amount, memo): Promise<any> {
    if (this.auth) {
      if (contract === 'dars.token') {
        return new Promise((resolve, reject) => {
          this.dars['transfer'](from, to, amount, memo, (err, trx) => {
            console.log(err, trx);
            if (err) {
              reject(JSON.parse(err));
            } else {
              resolve(true);
            }
          });
        });
      } else {
        return new Promise((resolve, reject) => {
          this.dars['contract'](contract, (err, tokenContract) => {
            if (!err) {
              if (tokenContract['transfer']) {
                const options = {authorization: from + '@active'};
                tokenContract['transfer'](from, to, amount, memo, options, (err2, trx) => {
                  console.log(err, trx);
                  if (err2) {
                    reject(JSON.parse(err2));
                  } else {
                    resolve(true);
                  }
                });
              } else {
                reject();
              }
            } else {
              reject(JSON.parse(err));
            }
          });
        });
      }
    }
  }

  checkPvtKey(k): Promise<any> {
    try {
      const pubkey = this.ecc['privateToPublic'](k);
      return this.loadPublicKey(pubkey);
    } catch (e) {
      console.log(e);
      return new Promise((resolve, reject) => {
        reject(e);
      });
    }
  }

  startMonitoringLoop() {
    if (!this.txMonitorInterval) {
      console.log('Starting monitoring loop!');
      this.txMonitorInterval = setInterval(() => {
        this.dars['getInfo']({}).then((info) => {
          const lib = info['last_irreversible_block_num'];
          if (this.txCheckQueue.length > 0) {
            console.log('Loop pass - LIB = ' + lib);
            this.txCheckQueue.forEach((tx, idx) => {
              console.log(tx);
              if (lib > tx.block) {
                this.dars['getTransaction']({id: tx.id}).then((result) => {
                  console.log(result.id);
                  if (result.id === tx.id) {
                    this.txh.push(result);
                    console.log(result);
                    this.txCheckQueue.splice(idx, 1);
                  }
                });
              }
            });
          } else {
            if (this.txMonitorInterval !== null) {
              console.log('Stopping monitoring loop!');
              clearInterval(this.txMonitorInterval);
              this.txMonitorInterval = null;
            }
          }
        });
      }, 500);
    } else {
      console.log('monitor is already polling');
    }
  }

}
