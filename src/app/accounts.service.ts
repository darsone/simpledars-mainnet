import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {DARSJSService} from './darsjs.service';
import {HttpClient} from '@angular/common/http';
import {BodyOutputType, Toast, ToasterService} from 'angular2-toaster';

import * as socketIo from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  public accounts: any[];
  public selected = new BehaviorSubject<any>({});
  public selectedIdx = 0;
  public lastUpdate = new Subject<any>();
  cmcListings = [];
  tokens = [];
  actions = [];
  totalActions: number;
  sessionTokens = {};
  allowed_actions = [];
  totalAssetsSum = 0;
  loading = true;
  private readonly socket: any;

  actionStore = {};

  static parseDRS(tk_string) {
    if (tk_string.split(' ')[1] === 'DRS') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }

  static parseDUSD(tk_string) {
    if (tk_string.split(' ')[1] === 'DUSD') {
      return parseFloat(tk_string.split(' ')[0]);
    } else {
      return 0;
    }
  }

  static extendAccount(acc) {
    let drs_balance = 0;
    let dusd_balance = 0;
    if (acc.tokens) {
      acc.tokens.forEach((tk) => {
		if (tk.split(' ')[1] === 'DRS') {
         drs_balance += AccountsService.parseDRS(tk);
		} else 
		if (tk.split(' ')[1] === 'DUSD') {
         dusd_balance += AccountsService.parseDUSD(tk);
		}
      });
    }
    let net = 0;
    let cpu = 0;
    if (acc['self_delegated_bandwidth']) {
      net = AccountsService.parseDRS(acc['self_delegated_bandwidth']['net_weight']);
      cpu = AccountsService.parseDRS(acc['self_delegated_bandwidth']['cpu_weight']);
    }
    return {
      name: acc['account_name'],
      drs_balance: Math.round((drs_balance) * 100000000) / 100000000,
      dusd_balance: Math.round((dusd_balance) * 100) / 100,
      staked: net + cpu,
      details: acc
    };
  }

  constructor(private http: HttpClient, private dars: DARSJSService, private toaster: ToasterService) {
    this.accounts = [];
    this.allowed_actions = ['transfer', 'voteproducer', 'undelegatebw', 'delegatebw'];

    this.dars.online.asObservable().subscribe(value => {
      if (value) {
        const store = localStorage.getItem('actionStore.' + this.dars.chainID);
        if (store) {
          this.actionStore = JSON.parse(store);
        }
      }
    });

//    this.socket.on('action', (data) => {
//      if (!this.actionStore[data.account]) {
//        this.actionStore[data.account] = {
//          last_gs: 0,
//          actions: []
//        };
//      }
//
//      this.actionStore[data.account]['last_gs'] = data.data.receipt.global_sequence;
//      const idx = this.actionStore[data.account]['actions'].findIndex((v) => {
//        return v.receipt.act_digest === data.data.receipt.act_digest;
//      });
//      if (idx === -1) {
//        this.actionStore[data.account]['actions'].push(data.data);
//        this.totalActions = this.actionStore[data.account]['actions'].length;
//      }
//    });
  }

  registerSymbol(data, contract) {
    const idx = this.tokens.findIndex((val) => {
      return val.name === data['symbol'];
    });
    let price = null;
    let usd_value = null;
    if (data['price']) {
      price = data['price'];
      usd_value = data['usd_value'];
    }
    if (idx === -1) {
      const obj = {
        name: data['symbol'],
        contract: contract,
        balance: data['balance'],
        precision: data['precision'],
        price: price,
        usd_value: usd_value
      };
      this.sessionTokens[this.selectedIdx].push(obj);
      this.tokens.push(obj);
    }
  }

  calcTotalAssets() {
    let totalSum = 0;
    this.tokens.forEach(tk => {
      if (tk.price) {
        totalSum = totalSum + (tk.balance * tk.price);
      }
    });
    this.totalAssetsSum = totalSum;
  }

  fetchTokens(account) {
    this.sessionTokens[this.selectedIdx] = [];
  }

  getTokenBalances() {
    this.tokens.forEach((tk, index) => {
      if (this.tokens[index]) {
        this.fetchTokenPrice(tk.name).then((price) => {
          this.tokens[index]['price'] = price;
        });
      }
    });
  }

  processAction(act, id, block_num, date) {
    const contract = act['account'];
    const action_name = act['name'];
    let symbol = '', user = '', type = '', memo = '';
    let votedProducers = null, proxy = null, voter = null;
    let cpu = 0, net = 0, amount = 0;

    if (action_name === 'transfer') {
      if (contract === 'dars.token') {
        // NATIVE TOKEN
        amount = act['data']['quantity']['split'](' ')[0];
        symbol = 'DRS';
      } else {
        // CUSTOM TOKEN
        amount = act['data']['quantity']['split'](' ')[0];
        symbol = act['data']['quantity']['split'](' ')[1];
      }
      memo = act['data']['memo'];
      if (act['data']['to'] === this.selected.getValue().name) {
        user = act['data']['from'];
        type = 'received';
      } else {
        user = act['data']['to'];
        type = 'sent';
      }
    }

    if (contract === 'dars' && action_name === 'voteproducer') {
      votedProducers = act['data']['producers'];
      proxy = act['data']['proxy'];
      voter = act['data']['voter'];
      type = 'vote';
    }

    if (contract === 'dars' && action_name === 'undelegatebw') {
      cpu = parseFloat(act['data']['unstake_cpu_quantity'].split(' ')[0]);
      net = parseFloat(act['data']['unstake_net_quantity'].split(' ')[0]);
      amount = cpu + net;
      user = act['data']['from'];
      type = 'unstaked';
    }

    if (contract === 'dars' && action_name === 'delegatebw') {
      cpu = parseFloat(act['data']['stake_cpu_quantity'].split(' ')[0]);
      net = parseFloat(act['data']['stake_net_quantity'].split(' ')[0]);
      amount = cpu + net;
      user = act['data']['from'];
      type = 'staked';
    }

    let valid = true;
    if (action_name === 'transfer') {
      if (act['data']['to'] === 'dars.stake') {
        valid = false;
      }
    }
    const obj = {
      id: id,
      type: type,
      action_name: action_name,
      contract: contract,
      user: user,
      block: block_num,
      date: date,
      amount: amount,
      symbol: symbol,
      memo: memo,
      votedProducers: votedProducers,
      proxy: proxy,
      voter: voter
    };
    this.actions.unshift(obj);
  }

  getAccActions(account, reload) {
    if (account === null) {
      account = this.selected.getValue().name;
    }
    this.actions = [];
    let last_gs = -1;
    if (this.actionStore[account]) {
      last_gs = this.actionStore[account]['last_gs'];
    }

    let limited = true;
    if (!this.actionStore[account]) {
      limited = false;
    } else {
      if (!this.actionStore[account]['last_gs']) {
        limited = false;
      }
    }

    if (reload) {
      last_gs = 0;
    }

//    this.socket.emit('get_actions', {
//      account: account,
//      limited: limited,
//      last_gs: last_gs
//    }, (results) => {
//      console.log('Stream output: ', results);
//
//      if (results === 'end') {
//
//        this.actionStore[account]['actions'].sort((a: any, b: any) => {
//          const dB = new Date(b.block_time).getTime();
//          const dA = new Date(a.block_time).getTime();
//          return dA - dB;
//        });
//
//        const payload = JSON.stringify(this.actionStore);
//        localStorage.setItem('actionStore.' + this.dars.chainID, payload);
//
//        this.actionStore[account]['actions'].forEach((action) => {
//          this.processAction(action['act'], action['trx_id'], action['block_num'], action['block_time']);
//        });
//
//        this.totalActions = this.actionStore[account]['actions'].length;
//        this.accounts[this.selectedIdx]['actions'] = this.actions;
//        this.calcTotalAssets();
//      }
//    });
  }

  reloadActions(account, reload) {
    console.log('reloading actions: ' + reload);
    if (account) {
//      this.socket.emit('close_actions_cursor', {
//        account: account
//      }, () => {
//        this.socket.emit('open_actions_cursor', {
//          account: account
//        }, (result2) => {
//          console.log(result2);
//          this.getAccActions(account, reload);
//        });
//      });
    }
  }

  getPublicKey(permissions, keytype) {
	  for (var i=0; i < permissions.length; i++) {
	   console.log("getPublicKey: checking perm_name=", permissions[i]["perm_name"]);
	   if (permissions[i]["perm_name"] == keytype) {
	    console.log("getPublicKey: key=", permissions[i].required_auth.keys[0].key);
	    return(permissions[i].required_auth.keys[0].key);
	   }
	  }
	  return("");
  }  
  
  select(index) {
    const sel = this.accounts[index];
    this.loading = true;
    this.tokens = [];
    if (sel['actions']) {
      if (sel.actions.length > 0) {
        this.actions = sel.actions;
      }
    } else {
      this.actions = [];
    }
    this.selectedIdx = index;
    this.selected.next(sel);

    const pbk = this.getPublicKey(this.selected.getValue().details.permissions, 'active');
    const stored_data = JSON.parse(localStorage.getItem('dars_keys.' + this.dars.chainID));

//    this.socket.emit('open_actions_cursor', {
//      account: this.selected.getValue().name
//    }, (result) => {
//      console.log(result);
//    });
    this.fetchTokens(this.selected.getValue().name);
  }

  initFirst() {
    this.select(0);
  }

  importAccounts(accounts) {
    const chain_id = this.dars.chainID;
    const payload = {
      importedOn: new Date(),
      updatedOn: new Date(),
      accounts: accounts
    };
console.log("importAccounts: payload=[",payload,"]");	
    localStorage.setItem('simpledars.accounts.' + chain_id, JSON.stringify(payload));
    this.loadLocalAccounts(accounts);
  }

  appendNewAccount(account) {
    const chain_id = this.dars.chainID;
    let payload = JSON.parse(localStorage.getItem('simpledars.accounts.' + chain_id));
    if (!payload) {
      payload = {
        accounts: [account],
        updatedOn: new Date()
      };
    } else {
      payload.accounts.push(account);
      payload['updatedOn'] = new Date();
    }
    localStorage.setItem('simpledars.accounts.' + chain_id, JSON.stringify(payload));
    this.loadLocalAccounts(payload.accounts);
  }

  appendAccounts(accounts) {
    const chain_id = this.dars.chainID;
    const payload = JSON.parse(localStorage.getItem('simpledars.accounts.' + chain_id));
    accounts.forEach((account) => {
      const idx = payload.accounts.findIndex((el) => {
        return el.name === account.account_name || el.account_name === account.account_name;
      });
      if (idx === -1) {
        payload.accounts.push(account);
      } else {
        const toast: Toast = {
          type: 'info',
          title: 'Импорт',
          body: 'Кошелек ' + account.account_name + ' уже импортирован!',
          timeout: 10000,
          showCloseButton: true,
          bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toaster.popAsync(toast);
      }
    });
    payload.updatedOn = new Date();
    localStorage.setItem('simpledars.accounts.' + chain_id, JSON.stringify(payload));
    this.loadLocalAccounts(payload.accounts);
  }

  loadLocalAccounts(data) {
    if (data.length > 0) {
      this.accounts = [];
      data.forEach((acc_data) => {
        acc_data.tokens = [];
        if (!acc_data.details) {
          this.accounts.push(AccountsService.extendAccount(acc_data));
        } else {
          this.accounts.push(acc_data);
        }
      });
      this.refreshFromChain();
    }
  }

  refreshFromChain(): void {
    const PQ = [];
    this.accounts.forEach((account, idx) => {
      const tempPromise = new Promise((resolve, reject) => {
        this.dars.getAccountInfo(account['name']).then((newdata) => {
          this.dars.getTokens(account['name']).then((tokens) => {
              let drs_balance = 0;
              let dusd_balance = 0;
              tokens.forEach((tk) => {
				if (tk.split(' ')[1] === 'DRS') {  
                 drs_balance += AccountsService.parseDRS(tk);
				} else
				if (tk.split(' ')[1] === 'DUSD') {  
                 dusd_balance += AccountsService.parseDUSD(tk);
				}
              });
              this.accounts[idx].name = account['name'];
              this.accounts[idx].drs_balance = Math.round((drs_balance) * 100000000) / 100000000;
              this.accounts[idx].dusd_balance = Math.round((dusd_balance) * 100) / 100;
              this.accounts[idx].staked = 0;
              this.accounts[idx].unstaking = 0;
              this.accounts[idx].unstakeTime = 0;
              this.accounts[idx].details = newdata;
              this.lastUpdate.next({
                account: account['name'],
                timestamp: new Date()
              });
              resolve();
          }).catch((error2) => {
            console.log('Error on getTokens', error2);
            reject();
          });
        }).catch((error1) => {
          console.log('Error on getAccountInfo', error1);
          reject();
        });
      });
      PQ.push(tempPromise);
    });
	
	if (typeof this.selected.getValue().name == 'undefined')
	 {
	  this.initFirst();	 
	  console.log("Current selected account is ",this.selected.getValue().name);
	 }
	
    Promise.all(PQ).then(() => {
      this.fetchTokens(this.selected.getValue().name);
      this.dars.storeAccountData(this.accounts);
    });
  }

  fetchListings() {
    this.http.get('https://api.coinmarketcap.com/v2/listings/').subscribe((result: any) => {
      this.cmcListings = result.data;
    });
  }

  fetchTokenPrice(symbol) {
    return new Promise((resolve, reject) => {
      let id = null;
      for (let i = 0; i < this.cmcListings.length; i++) {
        if (this.cmcListings[i].symbol === symbol) {
          id = this.cmcListings[i].id;
        }
      }
      if (id && symbol === 'DRSDAC') {
        this.http.get('https://api.coinmarketcap.com/v2/ticker/' + id + '/').subscribe((result: any) => {
          resolve(parseFloat(result.data.quotes.USD['price']));
        }, (err) => {
          reject(err);
        });
      } else {
        resolve(null);
      }
    });
  }
}
